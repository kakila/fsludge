\documentclass[10pt, a4paper]{elsarticle}
\usepackage[utf8]{inputenc}
\usepackage{textcomp}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{csquotes}
\usepackage[USenglish]{babel}

\usepackage[numbers]{natbib}
\usepackage{graphicx}
\usepackage{subcaption}

\usepackage[hyphens]{url}
\usepackage[hidelinks]{hyperref}
\hypersetup{breaklinks=true}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{abraces}
\usepackage{mathtools}
\usepackage{bm}

\usepackage[dvipsnames]{xcolor}

\usepackage{siunitx}

\usepackage{multirow}
\usepackage{colortbl}
\usepackage{rotating}

\newcommand{\ud}{\mathrm{d}}
\newcommand{\pder}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\dpder}[2]{\frac{\partial^2{#1}}{\partial{#2^2}}}
\newcommand{\sderp}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\newcommand{\tder}[2]{\frac{\ud{#1}}{\ud{#2}}}
\newcommand{\rot}[1]{\nabla \times {#1}}
\newcommand{\diver}[1]{\nabla \cdot {#1}}
\newcommand{\definter}[4]{\int_{#1}^{#2} {#3}\ud {#4}}
\newcommand{\inter}[2]{\int {#1}\ud {#2}}
\newcommand{\braket}[2]{\langle {#1} , {#2} \rangle}
% Misc
\newcommand{\eval}[1]{\Big |_{#1}}
\newcommand{\bset}[1]{\big\lbrace {#1} \big\rbrace}
\newcommand{\stimes}[2]{{{#1}\!\times\!{#2}}}
\newcommand{\trp}{\top}
\newcommand{\preup}[2]{{}^{#2}\!{#1}}
\newcommand{\datap}[1]{\mathtt{#1}}

% Operators
\DeclareMathOperator*{\armin}{arg\,min}
\DeclareMathOperator*{\armax}{arg\,max}
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\cov}{cov}
\DeclareMathOperator*{\nullsp}{null}
\DeclareMathOperator*{\ccov}{{\color{red}k}}
\DeclareMathOperator*{\cmean}{{\color{blue}m}}
\DeclareMathOperator*{\smbox}{smooth\,box}
\DeclareMathOperator*{\smSQbox}{smoothSQ\,box}
\DeclareMathOperator*{\gaussian}{\mathcal{N}} % Gaussian distribution

% Logicals
\newcommand{\suchthat}{\big \backslash \;}

\graphicspath{{img/}}

\journal{Journal of Environmental Management}

%\title{Modelling quantities and qualities (Q\&Q) of faecal sludge in Hanoi, Vietnam and Kampala, Uganda for improved management solutions\\
%Supplementary material}
%\author{Miriam Englund, Juan Pablo Carbajal, Amédé Ferré, Magalie Bassan, An Thi Hoai Vu, Viet-Anh Nguyen, Linda Strande}

\begin{document}

\begin{abstract}
\section*{Highlights}
\begin{itemize}
    \item Modelling approach for quantities and qualities of faecal sludge
    \item Data analysis based on containment type increases accuracy
    \item Demographic, environmental and technical data can be used as predictors
    \item Modelling can increase the efficiency of collecting and analysing data
\end{itemize}
\end{abstract}

\begin{keyword}
Low-income \sep Fecal Sludge \sep Sanitation \sep Septage \sep Demographic \sep Wastewater
\end{keyword}

\begin{frontmatter}
\title{Modelling quantities and qualities (Q\&Q) of faecal sludge in Hanoi, Vietnam and Kampala, Uganda for improved management solutions.\\
Supplementary material}

\author[add1]{Miriam Englund}
\author[add2,add3]{Juan Pablo Carbajal}
\author[add1,add3]{Amédé Ferré}
\author[add1,add3]{Magalie Bassan}
\author[add4]{An Thi Hoai Vu}
\author[add4]{Viet-Anh Nguyen}
\author[add1]{Linda Strande}
\ead{linda.strande@eawag.ch}

\address[add1]{Eawag: Swiss Federal Institute of Aquatic Science and Technology, Sandec: Department of Water and Sanitation in Developing Countries, Dübendorf, Switzerland.}
\address[add2]{Eawag: Swiss Federal Institute of Aquatic Science and Technology, UWM: Department Urban Water Management, Dübendorf, Switzerland.}
\address[add3]{Ecole Polytechnique Fédérale de Lausanne, School of Architecture, Civil and Environmental Engineering, Laboratory for Environmental Biotechnology, Lausanne, Switzerland.}
\address[add4]{HUCE: Hanoi University of Civil Engineering, IESE: Institute of Environmental Science and Engineering, Hanoi, Vietnam.}

\end{frontmatter}
%\maketitle

\tableofcontents

\section{Exploratory analysis}

We describe the exploratory data analysis~\citep{tufte1983visual} performed on the two data sets, before diving into the modelling tasks.
The two data sets were acquired in the cities of Hanoi (69 samples) and Kampala (180 samples)~\citep{Strande2018}.
Each data sets contains categorical variables, that can be nominal, ordinal, or interval~\citep[see][conceptual explanation]{catvar}, and quantitative variables (continuous and discrete).
We deemed the definition of meaningful encodings to map categories to quantities too time demanding for the analysis at hand.
Hence, the exploration of categorical and quantitive variables was done separately, with the one exception of the independence test, for which we discretized (a.k.a. quantize or binned) continuous variables.

\subsection{linear relations?}
The set of continuous variables included quality measurements (dissolved oxygen, Ammonium concentration, total suspended solids, etc.) as well as quantity measurements (containment volume, period between consecutive emptying, truck capacity, etc.).
The aim of the exploration of these variables was to observe the degree of linearity (and "non-linearity") of the relation between pairs of these variables.
To do it, we plotted each variable against the other and evaluated linearity and "nonlinearity".
To evaluate the degree of linearity we used the Pearson's correlation coefficient, and for
"non-linearity" we used the distance correlation~\citep{Szekely2014}.
Pearson's correlation coefficient rapidly decays for point clouds that do not resemble lines,
while distance correlation is able to detect some more complicated cloud shapes, see Fig.~\ref{fig:corrcoefvsdistcorr} for a comparison of the two coefficients.
The values of the coefficients was used to "guide the eye" when looking at the plots.
The exploration allowed us to group variables that are related and also to know if a linear modeling approach would be adequate.

\begin{figure}[htpb]
  \captionsetup[subfigure]{justification=centering}
  \begin{subfigure}{\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{./img/corrcoef.png}
    \caption{\label{corrcoef} source: Pearson correlation coefficient, Wikimedia Commons}
  \end{subfigure}

  \begin{subfigure}{\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{./img/distcorr.png}
    \caption{\label{distcorr}source: Distance correlation, by Naught101 under CC BY-SA 3.0,  Wikimedia Commons}
  \end{subfigure}

  \caption{Pearson's correlation coefficient~(\subref{corrcoef}) and distance correlation~(\subref{distcorr}) for several point clouds \label{fig:corrcoefvsdistcorr}}
\end{figure}

For each variable that later on would be predicted by our models, i.e. the output variable (e.g. emptying frequency), we built a new input variable by linearly combining all the other input variables.
The coefficients of the linear combination were selected to be positive, add to one, and to maximize the distance correlation to the output variable.
The combined variables were z-scored to avoid biases introduced by the the units of measurements (e.g. mixing millimeters with kilometers).
The correlation achieved by these combined variables prepared our expectations about the performance of a model, that is, if the new predictor did not increased the correlation significantly, we expected that simple models will not excel in predicting the output variable.

\subsection{Independence analysis}
To exploit most of the information in the data sets we also performed an estimation of the dependence between categorical variables and the would-be output variables of our models.
The aim of the analysis is to decide if the data should be modeled altogether or first separated by categories (this resembles a decision tree~\citep{Safavian1991}) and then model each group independently.
The simplest test for dependence of categorical variables, among many~\citep{wiki:catanal}, is the $\chi^2$-test~\citep{wiki:chi2test}.
However since the output variables were continuous we first discretized them and created ordinal variables.

Ordinalization of a continuous variable is done by creating a histogram of the continuous variable using arbitrary bin sizes, and then considering the bin label as the new ordinal variable.
This creates a new variables that takes values form 1 to the number of bins.
There are many ways of ordinalizing a variable, one chooses one that either encodes prior beliefs or that minimizes the loss of information while still providing a minimum number of bins.
For example, the uniform bins ordinalization creates an interval variable, that conserves the ratio between the values of the original variable.
Another ordinalization uses bins centered at the $q$-quantiles of the original variable, the ordinal variable takes values between $1$ and $q-1$.

In our exploration we ordinalized with uniform bins, logarithmic bins and $q$-quantiles, and obtained the same $\chi^2$-test results.
This was done to avoid a potential bias caused by the ordinalization.

As an example, we show in Fig.~\ref{fig:chi2TS} the results for total solids (TS) as the output variable in the Kampala data set\footnote{\url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/s_TS_exploratory_Kampala.m}}.
From this we concluded that TS is linked with the containment type (CoTyp) and the water contents (Wacon).
However, the contingencies in Table~\ref{tab:contable} show that CoTyp and Wacon provide the same information for the independence analysis.
That is, if we built models splitting the TS values, we should use only one of these for the splitting.

\begin{table}[h]
\begin{tabular}{lcc}
 & No & Yes \\
Pit latrine & 64 & 11 \\
Septic tank & 6 & 79
\end{tabular}
\caption{\label{tab:contable} Contingency table of dependent variables CoTyp and WaCon}
\end{table}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\textwidth]{./img/chi2test_TS.png}
  \caption{\label{fig:chi2TS} P-values for the $\chi^2$-test on the ordinalized TS values.
  The null hypothesis of independence is rejected if the p-value is below $0.01$}
\end{figure}

\section{Model structure}

The general structure of all models used in the article is:

\begin{equation}
y(\vec{x}) = \cmean\left(\vec{x}\right) + f\left(\vec{x}\right) + \xi
\label{eq:model}
\end{equation}

\noindent where $y$ is the output to be modelled and $\vec{x}$ is the vector of inputs (descriptors).
The model consists of two functions $\cmean, f$ and a noise term $\xi$.
The mean function $\cmean$ describes the local mean value of the function built with the input variables.
The correction function is nonlinear and is built from a covariance function.
The remaining data that cannot be explained by the two functions is ascribed to a noise term.

This model structure is trained using a data set $\datap{X}$ of inputs values.
$\datap{X}$ is the matrix built from the $N$ input vectors in the data set,

\begin{equation}
\datap{X} = \begin{bmatrix}\vec{\datap{x}}_1 \\ \vec{\datap{x}}_2 \\ \vdots \\ \vec{\datap{x}}_N\end{bmatrix}.
\end{equation}

\noindent and the corresponding observed $\vec{\datap{y}}$ outputs, i.e. the i-$th$ training point is $\left(\vec{\datap{x}}_i, \datap{y}_i\right)$

For Scenario 2: emptying frequency, we had a good candidate for a mean function, hence to assure that $f$ is only a correction of $\cmean$, its maximum value is limited to a fraction of that of the mean function.
We set this constrain using a fraction of the maximum observed output,

\begin{equation}
\max_{\vec{x} \in \datap{X}} f(\vec{x}) \sim \smSQbox\left(0, r \max\left(\vec{\datap{y}}, 100\right)\right)
\label{eq:maxcov}
\end{equation}

\noindent where $\smSQbox$ is a distribution with quadratic decay in the log domain.
The first two arguments are the cutoff values, and the third one is a decay rate.
See fig.~\ref{fig:smbox} for a graphical depiction.
In our study the upper cutoff is a fraction of the maximum observed output, and the value of the fraction $r \in [0,1]$ is determined manually for each data set, in such a way that $\max f(\vec{x}) \approx 0.1 \max \cmean(\vec{x})$.

\begin{figure}[htpb]
  \includegraphics[width=0.9\textwidth]{smoothboxes.png}
  \caption{\label{fig:smbox} Smooth box priors with linear ($\smbox$) and cuadratic $\smSQbox$ decay in the log domain.
  Vertical lines indicate the cutoffs.}
\end{figure}

The implementation of this model structure is given in the files \texttt{TSgp.m}\footnote{\url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/TSgp.m}} and \texttt{inflowgp.m}\footnote{\url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/inflowgp.m}}, for TS and emptying frequency, respectively.

Additionally the online repository\footnote{\url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/s_inflow_constrained_correction.m}} contains the source code of a study on the effect of the constraint in eq.~\eqref{eq:maxcov}.
A snapshot of the results of this analysis is available at \url{https://kakila.bitbucket.io/fsludge/correction_amplitude/s_inflow_constrained_correction.html}

\subsection{Mean function}

In both scenarios (TS and emptying frequency) and cities (Kampala and Hanoi) the mean function $\cmean$ is linear

\begin{equation}
\cmean\left(\vec{x}\right) = \vec{\omega} \cdot \vec{x} := \omega_0 + \omega_1 x_1 + \ldots + \omega_n x_n
\end{equation}

\noindent where $n$ is the number of input variables and $\vec{\omega}$ is a vector of coefficients learned from the data.

\subsection{Correction function}

The structure of the correction function $f$ is given by

\begin{equation}
f\left(\vec{x}\right) = \ccov\left[\vec{x}, \datap{X}; \vec{\beta}\right] \cdot \vec{\alpha} := \ccov\left(\vec{x}, \vec{\datap{x}}_1; \vec{\beta}\right) \alpha_1 + \ldots + \ccov\left(\vec{x}, \vec{\datap{x}}_N; \vec{\beta}\right) \alpha_N
\end{equation}

\noindent where $\ccov$ is a covariance function with two input arguments, and parametrized with a vector of parameters $\vec{\beta}$.
The parameter vector $\vec{\beta}$ and the weights $\vec{\alpha}$ are learned from the data.

In this study we used the following Matérn covariance function with automatic relevance determination

\begin{align}
\ccov\left(\vec{x}, \vec{\datap{x}}; \vec{\beta}\right) = \beta_{n+1} \exp \left(-\sqrt{\sum^{n}_{i=1} \frac{\left(x_i - \datap{x}_i\right)^2}{\beta_i^2}}\right).
\end{align}

\noindent The first $n$ components of the vector of parameters $\vec{\beta}$ represent
characteristic lengths along the components of the input.
When one of these distances is large, the covariance function (and the correction function) become insensitive to the corresponding input component.
That is, input components (descriptors) with large lengths are not relevant to compute the output.
This is true when the scale of all input components are similar, otherwise the meaning of "large length" would be different for each component.
This is the reason why all input variables in all models are rescaled with some measure of the dispersion of the inputs in the data set.
In the article we used the mean absolute deviation from the median.
If $z_{ji}$ is the $j$-th sample of the $i$-th input variable in the data set, the input for the model is,

\begin{equation}
\datap{x}_{ji} = \frac{z_{ji} - \bar{z_{:i}}}{<\vert z_{ji} - \bar{z_{:i}}\vert>},
\end{equation}

\noindent where $z_{:i}$ is the set of all values of the $i$th variable in the data set, $\bar{u}$ is the median of $u$, and $<u>$ its mean value.

An example of this covariance function in 2 dimensions is given in fig.~\ref{fig:matern2d}

\begin{figure}[htpb]
  \includegraphics[width=0.9\textwidth]{matern2d.png}
  \caption{\label{fig:matern2d} Matérn covariance function in 2 dimensions.
  The length in the x-direction is $3$ and $0.25$ in the y-direction.}
\end{figure}


\subsection{Noise term}
The noise term in eq.~\eqref{eq:model} is used in two ways.
On the one hand, its minimum amplitude defines the expected noise of the observed output values in the data set.
Lacking prior knowledge on this amplitude, we estimated it from the data itself.
On the other hand, the noise term amplitude is used to evaluate the ability of the model to represent the data.
This means, that if the resulting noise amplitude is similar to the minimum value estimated from the data, the model is able to recover patterns present in the data.
If the noise amplitude is large, the model is not able to represent the data, and models it as random values.

The structure used for the noise term in the modelling of TS is different from the one used to model emptying frequency.
Due to the lack of prior knowledge on the TS data, we assumed that the noise distribution was Gaussian with zero mean, and the variance determined by the data.
\begin{equation}
\xi \sim \frac{1}{\sqrt{2 \pi \sigma^2}}\exp \left(-\frac{\xi^2}{2 \sigma^2}\right)
\end{equation}

\noindent The standard deviation $\sigma$ was given a smooth box prior distribution with linear decay, and cutoff values of \SI{1}{\gram\per\liter} and \SI{10}{\gram\per\liter}, for both cities.

In the case of emptying frequency, we used a zero mean Student's t distributed noise.
\begin{equation}
\xi \sim \frac{1}{\sqrt{\nu \pi \sigma^2}}\frac{\Gamma \left(\frac{\nu+1}{2}\right)}{\Gamma \left(\frac{\nu}{2}\right)} \left( 1 + \frac{\xi^2}{\nu \sigma^2} \right)^{-\frac{\nu+1}{2}},
\end{equation}

\noindent Both parameters were given a smooth box prior distribution, the same for both cities.
The prior on $\nu$ had cutoff values of $2$ and $N-1$ for $\nu$, and $\sigma$ had cutoffs of $1$ day and $100$ weeks.

\section{Reuse of the reported models}

\subsection{Structure of the models}

The mathematical structure of purely \textbf{data-driven} models does not have any explicit link with knowledge about the mechanisms that generated the data (mechanistic knowledge).
They tend to be models with the \emph{universal approximation} feature, i.e. in the limit of infinite data the model is granted to recover true relations in the data, a popular example are (deep) neural networks.

These type of models are not the best choice when data is sparse (small data sets), as they tend to over-fit.
In these cases strong regularization or constraints are needed, and these encode the mechanistic knowledge of the user about the process generating the data, e.g. the relations are functional, smooth, etc.
Additionally, these models do not provide any warranty on their predictions for new inputs that are beyond the span of the inputs used for training; that is, when we want to extrapolate to a different context.
In machine learning it is customary that data-driven models fallback to a constant prediction, e.g. the mean value of the training data, or zero; when inputs are outside of the span of the training data.

\emph{Mechanistic} models, on the other hand, explicitly encode our knowledge about the data generating process.
Physical laws are stereotypical examples of these type of models.
These models are not universal approximators; they can represent but a very restricted set of relations between inputs and outputs, and therefore they can be trained with sparse data.
In other words, a few well-crafted experimental results are enough to reliably compute parameter values, or to support rejection of the proposed model.
Additionally, models of mechanisms are robust against the change of context, i.e. changes in the distribution of the input variables.
In causal analysis this is know as the \emph{principle of independent mechanisms}, and states that the conditional distribution of each variable given its causes (i.e., its mechanism) does not inform or influence the other conditional distributions\citep{Peters2017}.
Hence the performance of the model (and its parameters) are not expected to change much if the distribution of inputs variables changes.
For example, the model for free-fall was discovered in isolated environments, but its usability is global and atemporal.

The models we used in our analysis are a combination of these two types of models.
The mean function is used to explicitly encode knowledge about the data generating process, e.g. emptying frequencies are given by the ratio between storage capacity and the number of users.
While the correction function is given the freedom to correct that using patterns in the data.
This correction is not completely free: a) we constraint its amplitude so it is only a small correction, b) the family of functions the correction can represent is constrained and given by the chosen covariance function.
Therefore these models could be used as such in different contexts. Their performance (or lack of) and the changes in parameter values (if re-calibrated) gives us valuable information about the underlying processes.

\subsection{New contexts (new data sets)}

In the presence of a new data set we recommend the following procedure:
\begin{enumerate}
  \item \textbf{Comparison of the input variables}.
  This can be done visually by comparing the histograms or boxplots of the new values of the input variables and their values reported here in.
  \begin{itemize}
    \item If the new values fall within or are reasonably similar (e.g. overlapping criteria) to the values in out data set, then if the model were correct, using the parameters reported in the tables below should give similar performance as the one observed in this report.
    If the model with the parameters reported herein does not perform well in the new dataset, it implies that our models had over-fitted our data, or that the proposed model structure is not correct or is unable to generalize correctly.
    \item If the new values are not similar to the ones in our data set, the user is encourage anyways to use the parameters reported here and report the performance of the model.
    The failure of the model to predict the new values with similar or better performance than in our case is very valuable information and should prompt us to revisit the model structure and the assumptions we have made.
    In the unexpected case of the model performing still well in the new context, we would have evidence to support the generality of the structure proposed in this report; potentially leading to a deeper understanding of the processes of faecal sludge generation in the field.
  \end{itemize}
  \item \textbf{Re-calibration of the model to the new dataset}.
  In any of the situations described above, there is value in the information obtained by re-calibrating the same model structures to the new data.
  Since change or constancy of fitted parameters also provides information and insights.
  The re-calibration of the models should be straight forward modification of the scripts published together with this report.
\end{enumerate}

%    • This requires first a visual inspection of the data, like our box plots in figure 1. The model is based on/generated from data, so new data has to be within the same range.
%    • If the data looks similar, then yes, the same equations can be used. If the data set looks totally different, then the model needs to be recalibrated. This can be done by editing the provided scripts.
%    • The value of the model that we generated, is that now people can test it with their data. If it works, then potentially we have discovered some interesting underlying factors that can be used to predict FS. If not, then maybe it confirms that the problem cannot be generalized.

\section{Kampala: Containment volume per user}
The coefficients of the mean function for the emptying frequency of pit latrines in Kampala is counter-intuitive, in that emptying frequency increases with increasing containment volumes.
One explanatory hypothesis is that because the number of users increases considerably faster than the containment volume, the emptying frequency increases with containment volume.
However, although the containment volume available per user does goes to zero for increasing number of users (Fig.~\ref{fig:volperuser}, the same is observed for septic tanks which enjoy a model that is in agreement with the assumptions leading to the mean function.
Moreover, the small value of the coefficient (for both containment types) indicates that the emptying frequency is not very sensitive to the containment volume, and its determination is hard.

\begin{figure}[htpb]
  \includegraphics[width=0.9\textwidth]{CoVolperUser_Kampala.png}
  \caption{\label{fig:volperuser} Containment volume per user in Kampala.
  The available volume per user tends to zero for larger number of users, supporting the explanatory hypothesis of the anomalous mean function for pit latrines, however the same is observed for septic tanks which do not have an anomalous mean function.}
\end{figure}

%\section{Values of parameters}
\renewcommand{\listtablename}{\section{Tables with parameter values for all models}}
\addtocontents{lot}{\textbf{Table no.}~\hfill\textbf{Page}\par\vspace{1em}}
  \newcolumntype{y}{>{\columncolor{GreenYellow}}c}

\listoftables

\begin{sidewaystable}
  %\begin{table}[htpb]
    \centering
    \caption{Acronyms of variables in Kampala TS and the values used for normalization; followed by model parameters (corresponding to normalized variables).
    Script \texttt{s\_TS\_gp\_Kampala.m}, link to online repository \url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/s_TS_gp_Kampala.m}.
    \label{tab:acronymsKTS}}

    \begin{tabular}{ll|cc|cc}
    Acronym & Variable & \multicolumn{2}{c}{Pit Latrine} & \multicolumn{2}{c}{Septic tank}\\
            &          &        Median & MAD             & Median & MAD\\
    \hline
    \#U & Number of users & 1.50e1 & 1.12e1 & 1.00e1 & 9.58\\
    CV  & Containment Volume & 5.00 & 4.79 & 8.00 & 8.89\\
    TE  & Time since last emptied & 4.00e1 & 4.54e1 & 1.04e2 & 1.55e2\\
    IL  & Income level & 2.00 & 2.96e-1 & 4.00 & 7.88e-1\\
    \end{tabular}

    \vspace{1em}

    \begin{tabular}{l|yyyyy|yyyyy|y}
    \rowcolor{White}
    Technology & \multicolumn{5}{c|}{Mean function} & \multicolumn{5}{c|}{Covariance function} & Noise\\
    \rowcolor{White}
               & \#U & CV & TE & IL & $\omega_0$ & \#U & CV & TE & IL & $\beta_5$ & $\sigma$\\
    \hline
    \rowcolor{White}

    Pit latrine  & 3.07e-1 & -4.35 & \textbf{6.21} & 4.32e-1 & 3.10e1
    & \textbf{4.6e-3} & 1.6 & 3.0e-1 & 4.0 & 1.5e1
    & 9.63\\

    Septic tank & -1.60 & -1.00 & \textbf{4.93} & -1.25 & 1.62e1
    & 4.1e-1 & 1.5e4 & 9.2e-1 & \textbf{2.5e-4} & 1.0e1
    & 1.00e1
    \end{tabular}
%  \end{table}
\end{sidewaystable}
%% HANOI

  \begin{table}[htpb]
    \centering
    \caption{Acronyms of variables in Hanoi TS and the values used for normalization; followed by model parameters (corresponding to normalized variables).
    Script \texttt{s\_TS\_gp\_Hanoi.m}, link to online repository \url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/s_TS_gp_Hanoi.m}.
    \label{tab:acronymsHTS}}
    \begin{tabular}{ll|cc}
    Acronym & Variable &  Median & MAD \\
    \hline
    \#U & Number of users & 4.00 & 1.02\\
    CV  & Containment Volume & 1.00 & 0.467\\
    SA  & Sludge Age & 10.00 & 3.51\\
    PV  & Pumped Volume & 0.50 & 0.289\\
    \end{tabular}

    \vspace{1em}

    \begin{tabular}{l|cccc|cccc|c}
    Technology & \multicolumn{4}{c|}{Mean function} & \multicolumn{4}{c|}{Covariance function} & Noise\\
               & \#U & CV & SA & $\omega_0$ & \#U & SA & PV & $\beta_4$ & $\sigma$\\
    \hline
    Septic tank & -2.04 & \textbf{-5.94} & 3.97 & 37.6 &
    \textbf{4.2e-2} & 6.3e-2 & 2.2e-1 & 9.5 & 9.5
    \end{tabular}
  \end{table}

\begin{sidewaystable}
%  \begin{table}[htpb]
    \centering
    \caption{Acronyms of variables in Kampala emptying frequency and the values used for normalization in the $\log_{10}$ scale; followed by model parameters (corresponding to normalized $\log_{10}$ variables).
    Script \texttt{s\_inflow\_gp\_Kampala.m}, link to online repository  \url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/s_inflow_gp_Kampala.m}.
    \label{tab:acronymsKEF}}

    \begin{tabular}{ll|cc|cc}
    Acronym & Variable & \multicolumn{2}{c}{Pit Latrine} & \multicolumn{2}{c}{Septic tank}\\
            &          &        Median & MAD             & Median & MAD\\
    \hline
    \#U & Number of users & 1.18 & 2.41e-1 & 1.04 & 2.85e-1\\
    CV  & Containment Volume & 6.99e-1 & 2.57e-1 & 9.03e-1 & 2.85e-1\\
    CA  & Containment Age & 8.75e-1 & 2.20e-1 & 8.89e-1 & 3.00e-1\\
    TV  & Truck volume & 6.02e-1 & 1.18e-1 & 6.99e-1 & 1.75e-1\\
    IL  & Income level & 3.01e-1 & 8.79e-2 & 6.02e-1 & 1.18e-1
    \end{tabular}

    \vspace{1em}

    \begin{tabular}{l|yyy|yyyyyy|yyy}
    \rowcolor{White}
    Technology & \multicolumn{3}{c|}{Mean function} & \multicolumn{6}{c|}{Covariance function} & \multicolumn{3}{c}{Noise}\\
    \rowcolor{White}
               & \#U & CV & $\omega_0$ & \#U & CV & CA & TV & IL & $\beta_6$ & $\nu$ & $\sigma$ & std. deviation\\
    \hline
    \rowcolor{White}

    Pit latrine & \textbf{1.51e-1} & 3.16e-2 & -1.61
    & 1.3e7 & 1.8e9 & 2.3e3 & \textbf{3.8e-5} & 1.7e-4 & 6.47e-2
    & 3.06 & 1.49e-1 & 8.71e-1\\

    Septic tank & \textbf{3.48e-1} & -1.01e-1 & -1.93
    & 1.0e-3 & 2.3 & 2.4e3 & 4.4e6 & \textbf{1.2e-5} & 1.3e-1
    & 3.04 & 1.70e-1 & 1.26\\
    \end{tabular}
%  \end{table}%
\end{sidewaystable}

%% HANOI

\begin{sidewaystable}
  %\begin{table}[htpb]
    \centering
    \caption{Acronyms of variables in Hanoi emptying frequency and the values used for normalization in the $\log_{10}$ scale; followed by model parameters (corresponding to normalized $\log_{10}$ variables).
    Variables with zero values are shifted by 1 before taking logarithms, as indicated in the table.
    Script \texttt{s\_inflow\_gp\_Hanoi.m}, link to online repository \url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/s_inflow_gp_Hanoi.m}.
    \label{tab:acronymsHEF}}
    \begin{tabular}{ll|cc}
    Acronym & Variable &  Median & MAD \\
    \hline
    \#U & Number of users & 6.02e-1 & 1.01e-1\\
    CV  & Containment Volume & 0.00 & 1.34e-1\\
    TV  & Truck volume (+1) & 2.79e-1 & 3.35e-2\\
    WA  & Water Added volume (+1) & 1.68 & 2.57e-1\\
    SV  & Sludge emptied Volume (+1) & 1.77e-1 & 6.97e-2\\
    \end{tabular}

    \vspace{1em}

    \begin{tabular}{l|ccc|cccccc|ccc}
    Technology & \multicolumn{3}{c|}{Mean function} & \multicolumn{6}{c|}{Covariance function} & \multicolumn{3}{c}{Noise}\\
               & \#U & CV & $\omega_0$ & \#U & CV & TV & WA & SV & $\beta_6$ & $\nu$ & $\sigma$ & std. deviation\\
    \hline
    Septic tank
 & \textbf{-4.74e-2} & 1.25e-2 & -2.66
 & 9.4e2 & \textbf{2.3e-6} & 1.0 & 2.04e-1 & 1.0 & 1.0e-1
 & 3.06 & 4.91e-2 & 2.99e-1
    \end{tabular}
%  \end{table}
\end{sidewaystable}

\begin{sidewaystable}
%  \begin{table}[htpb]
    \centering
    \caption{Acronyms of variables in Households emptying frequency and the values used for normalization in the $\log_{10}$ scale; followed by model parameters (corresponding to normalized $\log_{10}$ variables).
    Script \texttt{s\_inflow\_gp\_Household.m}, link to online repository \url{https://gitlab.com/kakila/fsludge/tree/master/mfiles/s_inflow_gp_Household.m}.
    \label{tab:acronymsHHEF}}
    \begin{tabular}{ll|cc}
    Acronym & Variable &  Median & MAD \\
    \hline
    \#U & Number of users & 1.00 & 3.50e-1\\
    CV  & Containment Volume & 4.77e-1 & 3.98e-1\\
    TV  & Truck volume & 4.77e-1 & 3.11e-1
    \end{tabular}

    \vspace{1em}

    \begin{tabular}{l|cccc|cccc|ccc}
    Technology & \multicolumn{3}{c|}{Mean function} & \multicolumn{4}{c|}{Covariance function} & \multicolumn{3}{c}{Noise}\\
               & \#U & CV & $\omega_0$ & \#U & CV & TV & $\beta_4$ & $\nu$ & $\sigma$ & std. deviation\\
    \hline
    Septic tank
     & \textbf{3.82e-1} & 7.44e-2 & -2.02
     & 6.0e-4 & 1.0e4 & \textbf{1.6e-4} & 1.0e-1
     & 2.17 & 2.43e-1 & 8.79e-1
    \end{tabular}
%  \end{table}
\end{sidewaystable}

\clearpage

\addcontentsline{toc}{section}{References}
\bibliographystyle{unsrtnat}
\bibliography{references}

\end{document}
