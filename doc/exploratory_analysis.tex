In this example we describe the exploratory data analysis~\citep{tufte1983visual} performed on the two data sets, before diving into the modeling tasks.
The two data sets were acquired in the cities of Hanoi (69 samples) and Kampala (180 samples)~\citep{Strande2018}.
Each data sets contains categorical variables, that can be nominal, ordinal, or interval~\citep[see][conceptual explanation]{catvar}, and quantitative variables (continuous and discrete).
We deemed the definition of meaningful encodings to map categories to quantities too time demanding for the analysis at hand.
Hence, the exploration of categorical and quantitive variables was done separately, with the one exception of the independence test, for which we discretized (a.k.a. quantize or binned) continuous variables.

\section{linear relations?}
The set of continuous variables included quality measurements (dissolved oxygen, Ammonium concentration, total suspended solids, etc.) as well as quantity measurements (containment volume, period between consecutive emptying, truck capacity, etc.).
The aim of the exploration of these variables was to observe the degree of linearity (and "non-linearity") of the relation between pairs of these variables.
To do it, we plotted each variable against the other and evaluated linearity and "nonlinearity".
To evaluate the degree of linearity we used the Pearson's correlation coefficient, and for
"non-linearity" we used the distance correlation~\citep{Szekely2014}.
Pearson's correlation coefficient rapidly decays for point clouds that do not resemble lines,
while distance correlation is able to detect some more complicated cloud shapes, see Fig.~\ref{fig:corrcoefvsdistcorr} for a comparison of the two coefficients.
The values of the coefficients was used to "guide the eye" when looking at the plots.
The exploration allowed us to group variables that are related and also to know if a linear modeling approach would be adequate.

\begin{figure}[htpb]
  \captionsetup[subfigure]{justification=centering}
  \begin{subfigure}{\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{./img/corrcoef.png}
    \caption{\label{corrcoef} source: Pearson correlation coefficient, Wikimedia Commons}
  \end{subfigure}

  \begin{subfigure}{\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{./img/distcorr.png}
    \caption{\label{distcorr}source: Distance correlation, by Naught101 under CC BY-SA 3.0,  Wikimedia Commons}
  \end{subfigure}

  \caption{Pearson's correlation coefficient~(\subref{corrcoef}) and distance correlation~(\subref{distcorr}) for several point clouds \label{fig:corrcoefvsdistcorr}}
\end{figure}

For each variable that later on would be predicted by our models (e.g. period between consecutive emptying), i.e. the output variable, we built a new input variable by linearly combining all the other variables.
The coefficients of the linear combination were selected to be positive, add to one, and to maximize the distance correlation to the output variable.
The combined variables were z-scored (remove the mean and divided by standard deviation, a.k.a. standarization) before doing this, to avoid biases introduced by the the units of measurements (e.g. mixing millimeters with kilometers).
The correlation achieved by these combined variables prepared our expectations about the performance of a model, that is, if the new predictor did not increased the correlation significantly, we expected that simple models will not excel in predicting the output variable.

\section{Independence analysis}
To exploit most of the information in the data sets we also performed an estimation of the dependence between categorical variables and the would-be output variables of our models.
The aim of the analysis is to decide if the data should be modeled altogether or first separated by categories (this resembles a decision tree~\citep{Safavian1991}) and then model each group independently.
The simplest test for dependence of categorical variables, among many~\citep{wiki:catanal}, is the $\chi^2$-test~\citep{wiki:chi2test}.
However since the output variables were continuous we first discretized them and created ordinal variables.

Ordinalization of a continuous variable is done by creating a histogram of the continuous variable using arbitrary bin sizes, and then considering the bin label as the new ordinal variable.
This creates a new variables that takes values form 1 to the number of bins.
There are many ways of ordinalizing a variable, one chooses one that either encodes prior beliefs or that minimizes the loss of information while still providing a minimum number of bins.
For example, the uniform bins ordinalization creates an interval variable, that conserves the ratio between the values of the original variable.
Another ordinalization uses bins centered at the $q$-quantiles of the original variable, the ordinal variable takes values between $1$ and $q-1$.

In our exploration we ordinalized with uniform bins, logarithmic bins and $q$-quantiles, and obtained the same $\chi^2$-test results.
This was done to avoid a potential bias caused by the ordinalization.

As an example, we show in Fig.~\ref{fig:chi2TS} the results for total solids (TS) as the output variable in the Kampala data set.
From this we concluded that TS is linked with the containment type (CoTyp) and the water contents (Wacon).
However, the contingencies in Table~\ref{tab:contable} show that CoTyp and Wacon provide the same information for the independence analysis.
That is, if we built models splitting the TS values, we should use only one of these for the splitting.

\begin{table}[h]
\begin{tabular}{lcc}
 & No & Yes \\
Pit latrine & 64 & 11 \\
Septic tank & 6 & 79
\end{tabular}
\caption{\label{tab:contable} Contingency table of dependent variables CoTyp and WaCon}
\end{table}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.8\textwidth]{./img/chi2test_TS.png}
  \caption{\label{fig:chi2TS} P-values for the $\chi^2$-test on the ordinalized TS values.
  The null hypothesis of independence is rejected if the p-value is below $0.01$}
\end{figure}
