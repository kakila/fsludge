## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-25

## -*- texinfo -*-
## @defun {@var{} =} dataset (@var{}, @var{})
## Generate filtered datasets for further processing
## Load the CSv files and generate different views on the data
## suited for different studies
## @seealso{}
## @end defun

function [X Y Xcat XcatName] = dataset (in, out, city)

  if ischar (in) && nargin == 1
    # only city is given
    city = in;
    in = [];
    out = [];
  endif

  # Load dataset
  fin  = sprintf ("%s_NA-1.csv", city);
  data  = load (fullfile('..','data',fin));
  data (data < 0) = NA;  # -1 means NA
  N = size (data, 1);

  # Load metadata
  [headers, iscateg, categ] = metadata (city);
  if isempty(in) && isempty(out)
    # return metadata
    X = headers;
    Y = iscateg;
    Xcat = categ;
    return
  endif
  
  [idxin idxout] = iopartition (in, out, headers);

  Y = data(:, idxout);
  X = data(:, idxin);

  # Data with unobserved output values are removed
  YnotNA = all (isfinite (Y), 2);
  Y = Y(YnotNA,:);
  X = X(YnotNA,:);

  # Inputs with NA are removed
  XnotNA = find (all (isfinite (X), 2));
  X = X(XnotNA,:);
  Y = Y(XnotNA,:);

  # Indicate which inputs are categorical
  Xcat = iscateg(idxin);

  XcatName = [];
  if any(Xcat)
    tmpn = headers(idxin)(Xcat);
    for i=1:numel(tmpn)
      tmp.(tmpn{i}) = categ.(tmpn{i});
    endfor
    XcatName = tmp;
  endif

endfunction
