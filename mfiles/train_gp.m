## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-04-18

## -*- texinfo -*-
## @defun {@var{hyp} =} train_gp (@var{x}, @var{y}, @var{args}, @var{hyp})
## @defunx {@var{hyp} =} train_gp (@dots{}, @var{optimf}=[], @var{verbose}=false)
## @defunx {[@var{hyp} @var{loo}] =} train_gp (@dots{})
## Call GP optimizer without printing to the standard output
##
## The cell @var{args} contains the ordered argument list for the optimizer.
## This entails: inference method; mean, covariance, and likelihood function; 
## training inputs; and training outputs.
## See the GPML toolbox documentation for details.
##
## The optional input argument @var{optimf} is a handle to a valid minimizer.
## If empty then the default is @code{@@minimize_minfunc}.
##
## The optional input argument @var{verbose} indicates wether the call to the
## minimizer is allowed to print to the standard output.
##
## If a second output argument is requested, then the leave one out uncertainty
## is computed, for each training output.
##
## @seelaso{gp, minimize, minimize_minfunc, splitdata}
## @end defun

function [hyp, varargout] = train_gp (args, hyp, optimf=[], verbose=false)

  if isempty (optimf)
    optimf = @minimize_minfunc;
  endif
  if verbose
    hyp = optimf (hyp, @gp, -1e3, args{:});
  else
    evalc ('hyp = optimf (hyp, @gp, -Inf, args{:});');
  endif

  if nargout > 1
    # Compute actual LOO uncertainty
    x = args{end-1};
    y = args{end};
    N = size (y, 1);
    dy2 = zeros (N,1);
    args_ = args;
    for j=1:N
      [~,~,idxt,idxv] = splitdata ([x y], setdiff(1:N,j));
      args_(5:6)      = {x(idxt,:), y(idxt)};
      [~,dy2(j)]      = gp(hyp, args_{:}, x(idxv,:));
    endfor
    varargout{1} = 1.2 * sqrt (dy2);
  endif

endfunction
