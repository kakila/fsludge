## Visualization of some interesting relations in the data
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

clear all
close all

## Load data
# We load the number of users |NUsers| and the containment volume |CoVol|
# and observe some relation between these variables.
#
# Since the origin category |OrCat| can only be a cause of the observed variables
# we believe that we do not introduce bias in this way, in any case we are
# avoiding confounding.
#
Xname = {'NUsers','CoVol'};
Yname =  'CoTyp';

[X Y] = dataset (Xname, Yname, 'Kampala');
# Y in {'Pit latrine', 'Septic tank'}

NUsers = X(:,1);
CoVol  = X(:,2);
pit  = Y == 1;
tank = Y == 2;

##
#
#
figure (1)
plot (NUsers(pit), CoVol(pit) ./ NUsers(pit), 'o;Pit latrine;')
hold on
plot (NUsers(tank), CoVol(tank) ./ NUsers(tank), 'o;Septic tank;')
hold off
set (gca, 'xscale', 'log')
xlabel ('# users')
ylabel ('Containment V. per user')
grid on
axis tight
print (1, 'CoVolperUser_Kampala.png');
