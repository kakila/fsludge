## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-10-08

## -*- texinfo -*-
## @defun {[@var{hyp} @var{args}] =} inflowgp (@var{x}, @var{y}, @var{imean}, @var{hyp}=[], @var{noise}=[])
## @defunx {[@dots{}] =} inflowgp (@dots{}, @var{verbose}=false)
## Train a GP regressor with linear mean function, Matern covariance function
## (with automatic relevance detemrination), and t-distributed noise.
##
## Input: @var{x}. Outputs: @var{y}.
##
## The columns of @var{x} that should be used in the mean are indicated in the
## index array @var{imean}.
##
## If @var{hyp} is not empty, the it should define the iniitial values of the
## hyperparameters.
##
## The two element array @var{noise} is the range of acceptable noise intensities
## for the output. It will be used to constrain the varaince of the t-distribution.
##
## The hyperparatemers are constrained to increase the chance of convergence.
## That is, the nu parameter of the t-distribution is constrained from below to
## avoid too much amplification of the variance, and from above to avoid
## non-identifiability of solutions.
##
## The intensity of the covaraince function is limited to be a correction
## of the linear mean function. The maximum intensity of the correction is
## constrained by 5% of the maximum observed output.
##
## The input argument @var{verbose} controls the verbosity of the output.
##
## @seealso{train_gp, covMaternard, likT, priorSmoothBox, infPrior, infVB}
## @end defun

function [hyp args] = inflowgp (x, y, imean, hyp=[], noise=[], maxcov=[], verbose=false)

  linear_mean = {@meanSum, {@meanLinear, @meanConst}};
  meanf       = {@meanMask, imean, linear_mean};
  covf        = {@covMaternard, 1};
  likf        = {@likT};

  N     = length (y);  % number of samples
  numax = log (N - 1); % log of the maximum nu parameter for t-distribution
  dimX  = columns (x); % dimension of input

  % If not given, estimate the initial values of the model parameters
  nohyp = isempty (hyp);
  if nohyp
    hyp.lik  = [numax; 0];                 % t-distributed noise
    hyp.mean = [x(:,imean) ones(N,1)] \ y; % mean function, set to linear regression
    hyp.cov  = zeros(dimX + 1, 1);         % ARD lengths and variance set to 1
  endif

  %%% Prior distributions of model parameters
  % The nu parameter of the t-distribution is constrained to avoid
  % amplification of the variance.
  prior.lik = {{@priorSmoothBox1, log(3 - 1), numax, 40}, {}};
  % The intensity of the covariance function is limited to be a correction
  % of the linear mean function. The maximum intensity of the correction is
  % constrained by the maximum observed output
  prior.cov      = cell (dimX + 1, 1);
  if isempty (maxcov)
    maxcov         = log (max (abs (y)) * 0.05);
  endif
  prior.cov{end} = {@priorSmoothBox2, log(eps), maxcov, 100};
  if nohyp
    hyp.cov(end)   = maxcov;
  endif

  % If the user gave a noise interval, use it for the t-distributed noise
  % the values are scaled by the maximum value of the nu parameter.
  if !isempty (noise)
    prior.lik{end} = {@priorSmoothBox1, noise(1)-numax, noise(2)-numax, 40};
  endif

  % Inference of hyper-parameters is done using the Variational Bayes method
  infe = {@infPrior, @infVB, prior};

  args = {infe, meanf, covf, likf, x, y};
  hyp  = train_gp (args, hyp, [], verbose);

endfunction
