# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load statistics

## Category analysis of TS
# Load a few relevant categorical variables considering TS as the output
#
Xname = {'BioAdd', 'OrCat', 'SoWast', 'WaCon', 'CoTyp'};
Yname = 'TS';
[Xcat Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');
ncat = numel (Xname);

## Quantize TS
# Here we want to investigate the dependency between the categorical variables
# and the TS values.
# We use the Chi-square independence test instead of mutual information (or
# other information theoretic measures) due to the size of the dataset.
# The test works on categorical variables only, but TS is a continuous variable
# (at least up to the resolution of the measurement process).
# Hence we need to quantize TS to use the test.

##
# The quantization uses a linear scale for TS and uniform sized bins.
# other scales could be chosen, e.g. quantiles.
# We observed no difference in the results when using quantile or linear
# quantization.
#
scale = @(N) linspace (min(Y)-sqrt(eps), max(Y)+sqrt(eps), N);
% Alternative scale
% scale = @(N) quantile (Y, linspace (0,1,N));

##
# The maximum number of levels for TS is determined using the number of
# entries in the contingency table that are below 5.
#
nlvl = 2:20;
for i = 1:ncat
  for j = 1:length(nlvl)
    TS_lvl = scale (nlvl(j));
    TScat = lookup (TS_lvl, Y);
    T = crosstab (TScat, Xcat(:,i));

    bad_entries(i,j) = sum ((T < 5)(:));
  endfor % over levels
endfor % over variables

##
# Show the number of levels with valid contingency tables.
# We consider as valid contingency tables those that have more than
# 10% of their entries with at least 5 counts.
#
figure (1); clf;
tol = 0.1;
[~, ilvl] = min (bad_entries.' / length (Y) <= tol);
max_lvl = nlvl (ilvl-1);
bar (1:ncat, max_lvl, 'basevalue', nlvl(1));
axis tight
ylabel (sprintf('Max levels with %d%% valid entries',tol*100))
set (gca,'xticklabel', Xname)
set (gca,'ytick', nlvl)
nlvl = min (max_lvl);

## Independence test
# Although we are interested only on TS as output,.we create the matrix
# of p-values of the independence test for all pairs of variables.
#

% Add Ts categorical to set of variables
TS_lvl        = scale (nlvl);
Xcat(:,end+1) = lookup (TS_lvl, Y);
Xname{end+1}  = "TSCat";

##
# Test for independence between pairs of variables.
#
p_value = zeros (ncat+1);
for i = 1:ncat+1
  for j = i:ncat+1
    T = crosstab (Xcat(:,i), Xcat(:,j));
    p_value(i,j) = chisquare_test_independence (T);
    p_value(j,i) = p_value(i,j);
  endfor
endfor

% Set the significance level to 0.01
significance_lvl = 1e-2;

##
# The following plots show the dependence relation between the variables.
# The presence of the symbol indicates that the pair of variables are dependent
#
figure(2); clf
spy (p_value < significance_lvl);
set (gca, 'xticklabel', Xname);
set (gca, 'yticklabel', Xname);
set (gca, 'ytick', 1:ncat+1, 'xtick', 1:ncat+1);
title('Dependent categorical variables')

##
# We show 1-p-value for TS alone
#
figure(3); clf
bar (1:ncat, p_value(1:end-1,end),'facecolor', [1 0.7 0.7]);
axis([0.5 ncat+0.5 0 1])
set (gca, 'xtick', 1:ncat, 'xticklabel', Xname(1:end-1));
ylabel ('p-value (w.r.t. TS)')
line (axis()(1:2), significance_lvl*[1 1], 'linestyle', '--');
set (gca, 'ytick', [significance_lvl,1])
ix = find (p_value(1:end-1,end) < significance_lvl);
nx = length (ix);
text (ix, 0.5*ones(nx,1), 'Dependent', ...
  'horizontalalignment', 'center', 'fontweight', 'bold');

##
# From the previous analysis we conclude that TS is linked with CoTyp and Wacon.
# However, the following table indicates that CoTyp and Wacon provide the same
# information for the dependence analysis.
# That is, if we built models splitting the TS values, we should use only one
# of these for the splitting.
[~,iv] = ismember ({'CoTyp','WaCon'}, Xname);
vars   = Xname(iv);
nv     = numel (vars);

printf('\nContingency table of dependent variables %s\\%s\n\n',vars{1},vars{2});
T = crosstab (Xcat(:,iv(1)), Xcat(:,iv(2)));
printf ('\t\t%s\t%s\n',Xcat_str.(vars{2}){:})
printf ('%s\t%d\t%d\n',Xcat_str.(vars{1}){1}, T(1,:))
printf ('%s\t%d\t%d\n',Xcat_str.(vars{1}){2}, T(2,:))

##
# We also show the box plots of TS split using the two variables:
# the distributions are very similar
figure(2); clf
for i =1:nv
  subplot(nv, 1, i)
  idx_cat = categorypartition (Xcat(:,iv(i)));
  TSsplit.(vars{i}) = {Y(idx_cat{1}),Y(idx_cat{2})};
  boxplot (TSsplit.(vars{i}), 1);
  axis tight
  set (gca, 'xtick', [1 2], 'xticklabel', Xcat_str.(vars{i}))
  ylabel ('TS')
  legend (vars{i})
endfor
##
# To quantify the similarity of the distributions we perform a
# Two-sample Kolmogorov–Smirnov test
#
ks_significance_lvl = 1e-2;
printf ("\nKS statistic | Critical value | Same distr. (lvl=%.2f)?\n", ...
  ks_significance_lvl);
warnstate = warning ('off', 'Octave:statistics');
for i=1:2
  n = length(TSsplit.(vars{1}){i});
  m = length(TSsplit.(vars{2}){i});
  XX = TSsplit.(vars{1}){i};
  YY = TSsplit.(vars{2}){i};
  [p, ks, dv] = kolmogorov_smirnov_test_2 (XX, YY);
  crit = sqrt (- 0.5 * log (ks_significance_lvl * 0.5) * (n+m) / n / m);
  printf("%.2f\t\t%.2f\t\t%s\n", dv, crit, {'Yes','No'}{(dv>crit)+1});
endfor
warning(warnstate);
