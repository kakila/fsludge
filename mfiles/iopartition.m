## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-02-14

## -*- texinfo -*-
## @defun {[@var{idxin} @var{idxout}] =} iopartition (@var{in},@var{out},@var{headers})
## Return indexes of input output variables
## @end defun

function [idxin idxout] = iopartition (in, out, headers)

  #convert to cell if it isn't
  if !iscell(out)
    out = {out};
  endif
  
  [tf,idxin] = ismember (in, headers);
  if any(!tf)
    error ('Octave:invalid-input-arg', ...
    ['Input: ' sprintf("%s ", in{find(!tf)}) 'not found!']);
  endif
  [tf,idxout] = ismember (out, headers);
  if any(!tf)
    error ('Octave:invalid-input-arg', ...
    ['Output: ' sprintf("%s ", out{find(!tf)}) 'not found!']);
  endif

endfunction
