## Plot of Smooth Box priors
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load gpml

a = 1;
b = 10;
x  = linspace (a-2, b+2, 1e3).';
y  = priorSmoothBox1 (a, b, 40, x);
y2 = priorSmoothBox2 (a, b, 100, x);

figure (1), clf
  h = plot (x, [y y2],'-');
  set (h, 'linewidth', 4)
  axis tight
  ylim ([-50 1])
  ha = line ([a a], ylim, 'color', 'k');
  hb = line ([b b], ylim, 'color', 'k');
  legend ([h; ha], {'smooth box', 'smoothSQ box', 'cutoff'}, ...
    'location', 'northoutside', 'orientation', 'horizontal')
  xlabel ('x')
  ylabel ('log y')

