## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-11-12

## -*- texinfo -*-
## @defun {@var{} =} plotresults_gp (@var{}, @var{})
##
## @seealso{}
## @end defun

function [y_ dy_ Y_] = plotresults_gp (fig, hyp, args, X, Y, Xname, Yname, warp=@(x)x)

  if isstruct (args)
    field = fieldnames (args);
    nf    = numel (field);
    h     = cell (nf, 1);

    % figs 1 and 2
    for i=1:2
      figure (fig + i - 1);
      for j=1:nf
        h{j}(end+1) = subplot(1, nf, j);
        cla;
        title (field{j});
      endfor
    endfor

    % fig 3+
    for j=1:nf
      figure (fig + 2 + j - 1);
      h{j}(end+1) = axes();
      title (field{j});
    endfor

    y_ = dy_ = Y_ = struct();
    for i = 1:nf
        [_y_ _dy_ _Y_] = plotresults_gp (h{i}, ...
          hyp.(field{i}), args.(field{i}), ...
          X.(field{i}), Y.(field{i}), Xname, Yname, warp);
        y_.(field{i})  = _y_;
        dy_.(field{i}) = _dy_;
        Y_.(field{i})  = _Y_;
    endfor

    return
  endif

  x        = args{end-1};
  meanf    = args{2};
  mf       = feval (meanf{:}, hyp.mean, x);

  [y_ dy2 fm df2 lp post] = gp (hyp, args{:}, x);
  dy_      = 1.96 * sqrt (dy2);
  Y_       = warp (y_ + dy_ .* [0 -1 1]);
  y        = args{end};

  if numel(args{2}) > 1
    imean = args{2}{2};
  else
    imean = 1:numel(Xname);
  endif

  dimXmf   = length (imean);

  titles = {};
  if all(isaxes (fig))
    set_fig =@(x)axes(x);
    for i=1:length(fig)
      titles{i} = get (get (fig(i), 'title'), 'string');
    endfor
  else
    fig = fig + (0:2);
    set_fig=@(x)figure(x);
  endif

  set_fig(fig(1));
    [~, o] = sort (y_, 'descend');
    hv = errorbar (1:max(o), y_(o), dy_(o), '~or');
    hold on
    ht = plot (y(o), 'ob');
    set (ht, 'markerfacecolor', 'auto');
    set (hv, 'markerfacecolor', 'auto', 'markersize', 0.5*get(ht(1), 'markersize'))
    hold off
    axis tight
    xlabel ('rank (by predicted)')
    ylabel (sprintf ('%s %s', Yname{:}))
    if !isempty (titles) title (titles{1}); endif

  set_fig(fig(2));
    ht = errorbar (y, y_, dy_, '~ob');
    line([min(y) max(y)], [min(y) max(y)],'color','k');
    xlabel (sprintf('%s %s', Yname{:}));
    ylabel (sprintf('predicted %s  %s', Yname{:}));
    legend (ht, sprintf ('Corr coef.: %.2f', corr (y_, y)))
    axis tight
    if !isempty (titles) title (titles{2}); endif

  for j=3:length(fig)
    set_fig(fig(j));
      for i=1:dimXmf
        subplot (10, dimXmf, (i+dimXmf):dimXmf:(dimXmf*10))
        k = imean(i);
        X_ = X(:, k);
        ht = plot (X_, Y, 'o', 'markerfacecolor', 'auto');
        hold on
        hv = errorbar (X_, Y_(:,1), abs(Y_(:,2) - Y_(:,1)), ...
          abs(Y_(:,3) - Y_(:,1)), '~o');
        hold off
        set (hv, 'markersize', 1.5*get(hv, 'markersize'))
        xlabel (Xname(k));
        axis tight
        if i==1; ylabel (Yname{2}); endif
      endfor
    if !isempty (titles) subplot(10, dimXmf, 1:dimXmf); axis off; title (titles{j}); endif
  endfor
endfunction
