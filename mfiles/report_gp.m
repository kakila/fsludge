## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-11-12

## -*- texinfo -*-
## @defun {@var{} =} report_gp (@var{hyp}, @var{args})
## Report the results of the @command{lineargp} function
##
## The function prints a summary of the regression results.
##
## @seealso{lineargp}
## @end defun

function report_gp (hyp, args, warp=@(x)x)

  printf ('** Reports of results\n');
  nlml     = gp (hyp, args{:});
  printf ('Negative log marginal likelihod: %.2f\n', nlml);
  twolineprint('Mean function parameters', hyp.mean)

  x                       = args{end-1};  % trainig inputs
  y                       = args{end};  % trainig outputs
  [y_ dy2 fm df2 lp post] = gp (hyp, args{:}, x);
  dy_                     = sqrt (dy2);
  [N, dimX]               = size (x);

  meanf = args{2};      % mean function cell
  imean = 1:dimX;
  if numel(meanf) > 1
    imean = meanf{2};     % mean function mask indicating inputs to mean function
  endif
  covf = args{3};
  icov = 1:dimX;
  if strcmp (func2str (covf{1}), 'covMask')
    icov = covf{2}{1};     % cov function mask indicating inputs to cov function
  endif

  % minimum and maximum if inputs and mean function
  mf = feval (meanf{:}, hyp.mean, x);
  [minmf maxmf] = bounds (mf);
  [minx maxx] = bounds (x(:,imean));
  twolineprint ('Min of inputs', minx);
  twolineprint ('Max of inputs', maxx);
  printf ('Bounds mean fun: %.2f %.2f\n', minmf, maxmf);

  % Amplitude of the correction term given by the covariance function
  sigma2 = exp (2 * hyp.cov(end));
  printf ('Covariance amplitude: %.2f\n', sigma2);
  % Bounds of coefficents of variations and cov function (correction)
  Ks = feval(covf{:}, hyp.cov, x);
  cf = Ks * post.alpha;
  [mincf maxcf] = bounds (cf);
  printf ('Bounds cov fun: %.2f %.2f\n', mincf, maxcf);
  [mincv maxcv] = bounds (abs (cf ./ mf));
  printf ('Bounds coeff variation (%%): %.2f %.2f\n', mincv * 100, maxcv * 100);

  % Parameters of the t-distribution
  if strcmp ('likT', func2str (args{4}{1}))
    nu   = exp (hyp.lik(1)) + 1;
    sn   = exp (hyp.lik(2));
    printf ('t-distribution: %.2f %.2f\n', nu, sn);
  endif
  % Deviations in log10 and output space, respectively
  stdy = mean (dy_);                 % standard deviation in warped space
  devY = mean (warp (dy_));          % deviation in output space (not Gaussian)
  printf ('Deviations: %.2f %.2f\n', stdy, devY);
  % Correlation coefficient  predicted/true
  printf ('Corr coeff: %.2f\n', corr (y_, y));
  fflush (stdout);

endfunction

function twolineprint (s, x)
  printf ('%s\n', s);
  printf ('\t%.2f', x);
  printf ('\n');
endfunction
