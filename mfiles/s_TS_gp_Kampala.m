## Gaussian process regression TS from Kampala separated by origin category
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load gpml

## Gaussian Process regression of TS
# We load continuous site variables to build a model for TS.
# We also load the categorical variable container type to segment the dataset.
# The latter is then removed from the input variables |Xname|.
# |Yname| contains the output variable to be predicted.
#
Xname = {'NUsers','CoVol', 'SEmptyW', 'IC', 'CoTyp'};
Yname = 'TS';
[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');
Xcat      = X(:, isXcat);
ncat      = size (Xcat, 2);
X         = X(:,!isXcat);
Xname_cat = Xname(isXcat);
Xname     = Xname(!isXcat);

% Indexes to partition output using the categorical variable
idx_cat = categorypartition (Xcat);
cotyp   = Xcat_str.(Xname_cat{1}); %{'Pit latrine', 'Septic tank'};

ICOV = IMEAN = struct();
IMEAN.(cotyp{1}) = setdiff (1:length(Xname), []);
IMEAN.(cotyp{2}) = setdiff (1:length(Xname), []);
ICOV.(cotyp{1}) = setdiff (1:length(Xname), []);
ICOV.(cotyp{2}) = setdiff (1:length(Xname), []);

## GP regressor

if !exist('HYP', 'var')
  HYP = ARG = struct();
endif

% Verbosity is true, define the variable verbose in the command line to override
% Make sure verbose is false when generating a html report with publish
if ~exist ('verbose', 'var')
  verbose = false;
endif

% Loop over origin categories and build a model for each
for icat = 1:2
  cat_name = cotyp{icat};

  % We use the data directly, without non-linear mappings.
  % We only center to the median and rescale with the mean absolute deviation
  % from the median
  %
  % $$ x_i = \frac{X_i - \tilde{X}_i}{\alpha_{X_i}} $$
  %
  % The GP structure is defined in the function
  % <https://bitbucket.org/KaKiLa/fsludge_pub/src/tip/mfiles/TSgp.m _TSgp.m_>,
  % refer to it to know more details.
  x = X(idx_cat{icat},:);
  x = ( x - median (x) );
  x = x ./ mean (abs (x));
  assert (all (isfinite (x)))
  y = Y(idx_cat{icat});

  if !isfield (HYP, cat_name)
    hyp = [];
  else
    hyp = HYP.(cat_name);
  endif

  % Choose hyper-parameter constraints for each category
  TSerror = [log(1) log(10)]; % log of the error bounds, here 1-10 g/L
  switch cat_name
    case 'Pit latrine'
      maxcov = log (1.5e1);
    case 'Septic tank'
      maxcov = log (1e1);
  endswitch
  ifun = {IMEAN.(cat_name), ICOV.(cat_name)};
  [hyp args] = TSgp (x, y, ifun, hyp, TSerror, maxcov, verbose);

  % Store results for further plotting
  y_data.(cat_name) = y;
  HYP.(cat_name)    = hyp;
  ARG.(cat_name)    = args;
  XX.(cat_name)     = X(idx_cat{icat},:);
  YY.(cat_name)     = Y(idx_cat{icat},:);
endfor % over categories

## Summary of the results
# The coefficient of variation is computed as the ratio between the predictive
# standard deviation and the predictive mean.
#
# $$ c(\vec{x}) = \frac{\sigma_y(\vec{x})}{\bar{y}(\vec{x})} $$
#
# It is used to quantify the amount of correction.
#
# Since we do not have a prior model for |TS|, this correction indicates
# nonlinear terms that could be used to improve prediction.
#
yname = 'TS [g/L]';
xname = {'# users','Containment V.', 'Time since last emptied', 'Income level'};
for icat = 1:2
  cat_name = cotyp{icat};
  printf ('\n-- %s --\n', cat_name);
  printf ('Mean func vars:')
  printf (' %s,', xname{IMEAN.(cotyp{icat})}); printf('\n');
  printf ('Cov func vars:')
  printf (' %s,', xname{ICOV.(cotyp{icat})}); printf('\n');
  report_gp (HYP.(cat_name), ARG.(cat_name));
  % For tables
  printf('%s\n', cat_name)
  printf(' & %.4e', HYP.(cat_name).mean.'); printf('\n');       % mean
  printf(' & %.4e', exp(HYP.(cat_name).cov.')); printf('\n');   % cov
  printf(' & %.4e', exp(HYP.(cat_name).lik.')); printf('\n');   % lik
endfor

## Plot results
# These plots illustrate the performance of the model
#
plotresults_gp (1, HYP, ARG, XX, YY, xname, {'', yname});
print (2, 'Fig4_Kampala_TS_output.png');

##
# This plot shows the relative weight of each variable in the mean function
# and the relevance in the covariance function.
plothypARD (7, HYP.(cotyp{1}), xname, {IMEAN.(cotyp{1}), ICOV.(cotyp{1})});
subplot (2,1,1); title (cotyp{1})
print (7, 'Fig3a_Kampala_TS_covcoeffs.png');
plothypARD (8, HYP.(cotyp{2}), xname, {IMEAN.(cotyp{2}), ICOV.(cotyp{2})});
subplot (2,1,1); title (cotyp{2})
print (8, 'Fig3b_Kampala_TS_covcoeffs.png');
