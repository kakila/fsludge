## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-10-08

## -*- texinfo -*-
## @defun {[@var{hyp} @var{args}] =} TSgp (@var{x}, @var{y}, @var{imean}, @var{hyp}=[], @var{noise}=[])
## @defunx {[@dots{}] =} TSgp (@dots{}, @var{verbose}=false)
## Train a GP regressor with linear mean function, Matern covariance function
## (with automatic relevance determination), and normal-distributed noise.
##
## Input: @var{x}. Outputs: @var{y}.
##
## The columns of @var{x} that should be used in the mean are indicated in the
## index array @var{imean}.
##
## If @var{hyp} is not empty, the it should define the iniitial values of the
## hyperparameters.
##
## The two element array @var{noise} is the range of acceptable noise intensities
## for the output. It will be used to constrain the varaince of the normal distribution.
##
## The intensity of the covaraince function is limited to be a correction
## of the linear mean function. The maximum intensity of the correction is
## constrained by 5% of the maximum observed output.
##
## The input argument @var{verbose} controls the verbosity of the output.
##
## @seealso{train_gp, covMaternard, likGauss, priorSmoothBox, infPrior, infGauss}
## @end defun

function [hyp args] = TSgp (x, y, imean, hyp=[], noise=[], maxcov=[], verbose=false)

  if iscell (imean)
    icov = imean{2};
    imean = imean{1};
  endif

  linear_mean = {@meanSum, {@meanLinear, @meanConst}};
  meanf       = {@meanMask, imean, linear_mean};
  covf        = {@covMask, {icov, {@covMaternard, 1}}};
  likf        = {@likGauss};

  N   = length (y);  % number of samples

  dimX    = columns (x); % dimension of input
  dimXcov = length (icov);
  % If not given, estimate the initial values of the model parameters
  if isempty (hyp)
    hyp.lik = 0;
    hyp.mean = [x(:,imean) ones(N,1)] \ y; % mean function, set to linear regression
    hyp.cov  = zeros (dimXcov + 1, 1);     % ARD lengths set to 1
  endif

  % Prior distributions of model parameters
  % The intensity of the covaraince function is limited to be a correction
  % of the linear mean function. The maximum intensity of the correction is
  % constrained by 5% of the maximum observed output
  prior.cov      = cell (dimXcov + 1, 1);
  if isempty (maxcov)
    maxcov         = log (max (abs (y)) * 0.05);
  endif
  prior.cov{end} = {@priorSmoothBox1, log(eps), maxcov, 40};

  % If the user gave a noise interval, use it for the normal distributed noise
  if !isempty (noise)
    prior.lik = {{@priorSmoothBox1, noise(1), noise(2), 40}};
  endif

  % Inference of hyper-parameters is done using the exact method
  infe = {@infPrior, @infGaussLik, prior};

  args = {infe, meanf, covf, likf, x, y};
  hyp  = train_gp (args, hyp, [], verbose);

endfunction
