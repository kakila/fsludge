# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

## Gaussian Process regression of TS
# We load continuous site variables to build a model for TS
# We also load a few categorical variables to segment the dataset
#
Xname = {'CoVol', 'CoAge', 'NUsers', 'CoTyp'};
Yname = {'TS', 'SEmptyW'};
[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');
Xcat = X(:, isXcat);
ncat = size (Xcat, 2);
X = X(:,!isXcat);
Xname_cat = Xname(isXcat);
Xname = Xname(!isXcat);

### Partition of TS
##
categ = {'WaCon', 'CoTyp'}{2};
[~,iv] = ismember (Xname_cat, categ);
iv(iv==0) = [];
idx_cat = categorypartition (Xcat(:,iv));

## Map input variables
#
icat = 1;
x = X(idx_cat{icat},:);
y = Y(idx_cat{icat},:);

qin = 1 ./ y(:,2);
y = y(:,1) .* qin;
Yname = {'TS','Qin'};
x = log10 (x);
x = zscore (x);

## Build trainig and test set
#
[N dim] = size (x);
[yo o] = sort (log(y));
idx = o(round(linspace(2,N-1,20)));
[xtst xtrn tst trn] = splitdata (x, idx);
ytrn = y(trn);
ytst = y(tst);

## GP regressor
#
pkg load gpml
meanf = {@meanSum, {@meanLinear, @meanConst}};
covf = {@covScale,{@covMaternard,1}};
likf = {@likGauss};

##
#
prior.lik = {{@priorSmoothBox1,1,5,30}};
args = {{@infPrior,@infLOO,prior}, meanf, covf, likf, xtrn, ytrn};

##
#
if !exist ('hyp', 'var')
  hyp.lik  = 3;
  hyp.mean = 5 * randn (dim+1,1);
  hyp.cov  = randn (dim+1+1,1);
endif

##
#
if exist('calc_loo', 'var')
  [hyp, dy_loo] = train_gp (x, y, args, hyp);
else
  hyp = train_gp (x, y, args, hyp);
endif

##
#
nlml = gp (hyp, args{:});
[y_ dy2] = gp (hyp, args{:}, x);
dy = 1.2 * sqrt (dy2);
printf ("Results for %s*%s\n", Yname{:});
printf ("Negative log marginal likelihood: %.3f\n", nlml);
printf ("RMSE in test set (%d): %.3f\n", length(tst), sqrt(meansq(ytst-y_(tst))));
fflush (stdout);

##
#
figure (1)
clf
subplot(3,1,1)

hold on

ht = errorbar (ytrn, y_(trn), dy(trn), '~ob');
ht(2) = errorbar (ytst, y_(tst), dy(tst), '~og');

if exist('dy_loo', 'var')
  hv = errorbar (ytrn, y_(trn), dy_loo(trn), '~or');
  hv(2) = errorbar (ytst, y_(tst), dy_loo(tst), '~or');
  set(hv(1),'markerfacecolor','auto','markersize', 0.5*get(ht(1),'markersize'))
  set(hv(2),'markerfacecolor','auto','markersize', 0.3*get(ht(2),'markersize'))
endif

hold off

line([min(y) max(y)], [min(y) max(y)],'color','k');
xlabel (sprintf('log10 %s*%s',Yname{:}));
ylabel (sprintf('predicted log10 %s*%s', Yname{:}));
axis tight

subplot(3,1,2)
ell = abs(hyp.mean(1:dim));
L   = sum (ell);
ell ./= L;
[ell orel] = sort (ell, 'descend');
bar (1:dim, ell, 'r');
axis tight
set (gca, 'xticklabel', Xname(orel))
ylabel ('Relevance [mean]');

subplot(3,1,3)
# inverse ARD lengths
ell2 = exp (-2 * hyp.cov(1:dim));
L    = sum (ell2);
ell2 ./= L;
[ell2 orel] = sort (ell2, 'descend');
bar (1:dim, ell2, 'r');
axis tight
set (gca, 'xticklabel', Xname(orel))
ylabel ('Relevance [cov]');

figure(2)
clf
[~, o] = sort (y_,'descend');
[~, i] = ismember(tst,o);
ht = plot(y(o),'ob',i,y_(o)(i),'og');
hold on
hv = errorbar (1:max(o),y_(o), dy(o), '~or');
hv(2) = errorbar (i,y_(o)(i), dy(o)(i), '~.g');
set(hv(1),'markerfacecolor','auto','markersize', 0.5*get(ht(1),'markersize'))
hold off
axis tight
xlabel(sprintf('%s*%s rank (predicted)',Yname{:}))
ylabel(sprintf('%s*%s',Yname{:}));
legend (ht(1), Xcat_str.(Xname_cat{iv}){icat});
