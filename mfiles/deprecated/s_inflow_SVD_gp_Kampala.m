# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

## Gaussian Process regression
# We model the quality outputa using Gaussian Process regression with the
# Matern covariance function

Xname = {'NUsers','CoVol','CoAge','OrCat'};
Yname = 'SEmptyW';
[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');
Xcat = X(:, isXcat);
ncat = size (Xcat, 2);
assert (ncat, 1);
idx_cat = categorypartition (Xcat);
X = X(:,!isXcat);
Xname = Xname(!isXcat);

## Decorrrelate input variables
#
x = log10 (X);
xmean = mean (x);
[u s v] = svd (x - xmean, 1);
M   = s * v.';
Mri = v * diag (1 ./ diag (s)); # right inverse of M
val2data=@(x) x * M + xmean; # convert to data space
data2val=@(x) (x - xmean) * Mri; # convert to data space
par2data=@(z) Mri * z; # convert regression paramters to data space

x = u;
y = - log10 (Y);

[N dim] = size (x);

## GP regressor
#
pkg load gpml
mpoly1 = {@meanMask, 1, {@meanPoly, 2}};
meanf = {@meanSum, {mpoly1, {@meanMask, [2 3], {@meanLinear}}, @meanConst}};
covf = {@covProd, {{@covPoly,'ard',1}, @covNoise}};
likf = @likGauss;

if !exist('hyp','var')
  hyp.lik  = 0;
  hyp.mean = 10 * ones (2+2+1,1);
  hyp.cov  = [rand(dim+2,1); 0];
endif
prior.cov{length(hyp.cov)} = {@priorClamped};
args = {{@infPrior,@infLOO,prior}, meanf, covf, likf, x, y};

hyp = minimize (hyp, @gp, -1e3, args{:});
nlml = gp (hyp, args{:});
[y_ dy2] = gp(hyp, args{:}, x);
dy = 1.2 * sqrt (dy2);

# GET actual LOO uncertainty
if !exist('dy_loo','var')
  dy2 = zeros (N,1);
  args_ = args;
  for j=1:N
    [~,~,idxt,idxv] = splitdata ([x y], setdiff(1:N,j));
    args_(5:6)      = {x(idxt,:), y(idxt)};
    [~,dy2(j)]      = gp(hyp, args_{:}, x(idxv,:));
  endfor
  dy_loo = 1.2 * sqrt (dy2);
endif

printf ("Results for %s\n", Yname);
printf ("Negative log marginal likelihood %s: %.3f\n", Yname, nlml);
fflush (stdout);

figure (1)
clf
subplot(2,1,1)
ht = errorbar (y, y_, dy, '~ob');
hold on
hv = errorbar (y, y_, dy_loo, '~or');
set(hv,'markerfacecolor','auto','markersize', 0.3*get(ht,'markersize'))
hold off
line([min(y) max(y)], [min(y) max(y)],'color','k');
xlabel (sprintf('-log10(%s)',Yname));
ylabel (sprintf('predicted -log10(%s)', Yname));
axis tight

subplot(2,1,2)
# inverse ARD lengths
ell2 = diag(Mri * diag(exp(-2*hyp.cov(1:dim))) * Mri.');
L    = sum (ell2);
ell2 ./= L;
[ell2 o] = sort (ell2, 'descend');
bar (1:dim, ell2, 'r');
axis tight
set (gca, 'xticklabel', Xname(o))
ylabel ('Relative relevance');

figure (2)
clf
##
# Grid in the log space
clear X_
for i=1:dim
  tmp = log10(X(:,i));
  X_(:,i) = linspace (min (tmp), max (tmp), 5);
endfor
[X_1 X_2 X_3] = meshgrid (X_(:,1), X_(:,2), X_(:,3));
sz = size (X_1);
X_ = [X_1(:), X_2(:), X_3(:)];

x_ = data2val(X_);
[y_, dy_2] = gp (hyp, args{:}, x_);
dy_        = 1.2 * sqrt(dy_2);

y_  = reshape (y_, sz);
Y_ = 10.^ mean (y_,3);

X_1 = 10.^(X_1(:,:,1));
X_2 = 10.^(X_2(:,:,1));
hx = mesh (X_1, X_2, Y_, 'facecolor', 'none');
hold on
hd = plot3 (X(:,1), X(:,2), 1./Y, 'o', 'markerfacecolor', 'auto');
hold off
xlabel(Xname{1})
ylabel(Xname{2})
zlabel('Frequency [1/week]')
set (gca,'zscale', 'log', 'yscale','log','xscale','log')
axis tight

figure(3)
clf
ncat = numel(idx_cat);
nL = 20;
m = arrayfun (@(i)median(y(idx_cat{i})), 1:ncat);
[m, o] = sort (m);
symb = 'opv*+^';
col = flipud (viridis (ncat));
if !exist('plasma')
  matplotlib_cm('plasma')
endif
colormap (plasma(nL));
hold on
for i=1:ncat
  j = idx_cat{o(i)};
  hd(i) = loglog (X(j,1), X(j,2), symb(i), 'markerfacecolor', 'auto');
  set(hd(i),'color', col(i,:));
endfor
hx = contourf (X_1, X_2, mean (y_,3), nL);
hc = colorbar ();
set(hc,'ylabel',sprintf('-log10(%s)',Yname));
hold off
xlabel(Xname{1})
ylabel(Xname{2})
axis tight
Xcat_str.OrCat{1} = 'I-C-I';
Xcat_str.OrCat{2} = 'P.toilet';
Xcat_str.OrCat{3} = 'H+';
Xcat_str.OrCat{5} = 'H';
Xcat_str.OrCat{4} = 'Sc';
Xcat_str.OrCat{6} = 'R/Ho';
lbl = Xcat_str.OrCat(o);
for i=1:numel(lbl)
  lbl{i} = sprintf ("%s (%.1f)", lbl{i}, m(i));
endfor
legend (hd, lbl, 'Location','NorthOutside',...
                            'Orientation', 'Horizontal')
