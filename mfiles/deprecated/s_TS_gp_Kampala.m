# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load gpml

## Gaussian Process regression of TS
# We load continuous site variables to build a model for TS
# We also load the categorical variable container type to segment the dataset
#
Xname = {'SEmptyW','CoVol','CoAge','NUsers','CoTyp'};
Yname = 'TS';
[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');
Xcat = X(:, isXcat);
ncat = size (Xcat, 2);
X = X(:,!isXcat);
Xname_cat = Xname(isXcat);
Xname = Xname(!isXcat);

% Indexes to partition TS using the categorical variable
idx_cat = categorypartition (Xcat);

% all the regression is performed on nonlinearly transformed variables
X_ = log (X);
X_ = zscore (X_);
[N dim] = size (X_);

## GP regressor
# The regressor uses a linear mean function.
# We use a Matern covariance function with automatic relevance determination.
#
meanf = {@meanSum, {@meanLinear, @meanConst}};
covf = {@covMaternard,1};
likf = {@likGauss};

##
# To avoid over-fitting we set a prior on the noise parameter that penalizes
# values that are unrealistically low.
prior.lik = {{@priorSmoothBox1,2,5,40}};
args = {{@infPrior,@infExact,prior}, meanf, covf, likf, [], []};

## Regress the data
#
y_model = dy_model = idx_tst = idx_trn = struct ();
if !exist('HYP', 'var')
  HYP = struct();
endif

for icat = 1:2
  cat_name = Xcat_str.(Xname_cat{1}){icat};

  x = X_(idx_cat{icat},:);
  y = Y(idx_cat{icat});

  % If the hyper-parameters are not set, set some good initial values
  if !isfield (HYP, cat_name)
    hyp.lik  = 5; % start with a highly regularized model
    switch cat_name
      case 'Septic tank'
        hyp.mean = [8;-1.5;0;-4.5;47] + 3 * randn (dim+1,1);
        hyp.cov  = [3;11;13;0;3];
      case 'Pit latrine'
        hyp.mean = [5;-20;6;17;29] + 3 * randn (dim+1,1);
        hyp.cov  = [3;11;13;0;3];
    endswitch
  else
    hyp = HYP.(cat_name);
  endif

  % Training and test set.
  % We select test points evenly spaced in the TS scale
  ntest = 20;
  [N dim] = size (x);
  [yo o]  = sort (y);
  idx     = o(round (linspace (2, N-1, ntest)));

  [xtst xtrn tst trn] = splitdata (x, idx);
  ytrn = y(trn);
  ytst = y(tst);

  % input arguments for the regression function
  args{5} = xtrn; % training inputs
  args{6} = ytrn; % training outputs

  % If the variable _calc_loo_ is defined then we will manually calculate the
  % leave one out prediction error.
  if exist('calc_loo', 'var')
    [hyp, dy_loo] = train_gp (x, y, args, hyp);
  else
    hyp = train_gp (x, y, args, hyp);
  endif

  % Print some summary of the results
  nlml     = gp (hyp, args{:});
  [y_ dy2] = gp (hyp, args{:}, x);
  dy       = 1.96 * sqrt (dy2);
  rmse_test = sqrt (meansq (ytst - y_(tst)));
  printf ("\nResults for %s (%s)\n", Yname, cat_name);
  printf ("Negative log likelihood %s: %.3f\n", Yname, -nlml);
  printf ("RMSE in test set (%d): %.3f\n", length(tst), rmse_test);
  fflush (stdout);

  % Store results for further plotting
  y_data.(cat_name)   = y;
  y_model.(cat_name)  = y_;
  dy_model.(cat_name) = dy;
  idx_tst.(cat_name)  = tst;
  idx_trn.(cat_name)  = trn;

  HYP.(cat_name) = hyp;

endfor % over categories

## Plot results
# These plots illustrate the performance of the model
#
cotyp = {'Pit latrine', 'Septic tank'};

figure(1); clf;

function ht = do_plot_1 (y, y_, dy, tst)
  [~, o] = sort (y_, 'descend');
  [~, i] = ismember (tst, o);
  ht = plot (y(o), 'ob', i, y_(o)(i), 'og');
  hold on
  hv = errorbar (1:max(o), y_(o), dy(o), '~or');
  hv(2) = errorbar (i, y_(o)(i), dy(o)(i), '~.g');
  set(hv(1), 'markerfacecolor', 'auto', 'markersize', 0.5*get(ht(1), 'markersize'))
  hold off
endfunction

for i = 1:2
  subplot (1,2,i)
  y = y_data.(cotyp{i});
  y_ = y_model.(cotyp{i});
  dy = dy_model.(cotyp{i});
  tst = idx_tst.(cotyp{i});

  ht = do_plot_1 (y, y_, dy, tst);
  axis tight
  v = axis();
  axis([v(1:2) 0 v(4)]);
  xlabel(sprintf('%s rank (predicted)',Yname))
  if i == 1; ylabel(Yname); endif
  legend (ht(1), cotyp{i});
endfor

figure (2); clf;

function ht = do_plot_2 (y, y_, dy, tst, trn)
  ht = errorbar (y(trn), y_(trn), dy(trn), '~ob');
  hold on
  ht(2) = errorbar (y(tst), y_(tst), dy(tst), '~og');
  #if exist('dy_loo', 'var')
  #  hv = errorbar (ytrn, y_(trn), dy_loo(trn), '~or');
  #  hv(2) = errorbar (ytst, y_(tst), dy_loo(tst), '~or');
  #  set(hv(1),'markerfacecolor','auto','markersize', 0.5*get(ht(1),'markersize'))
  #  set(hv(2),'markerfacecolor','auto','markersize', 0.3*get(ht(2),'markersize'))
  #endif
  hold off
  mM = [min(y) max(y)];
  line (mM, mM,'color','k');
endfunction

for i=1:2
  subplot (1,2,i)
  y = y_data.(cotyp{i});
  y_ = y_model.(cotyp{i});
  dy = dy_model.(cotyp{i});
  tst = idx_tst.(cotyp{i});
  trn = idx_trn.(cotyp{i});

  ht = do_plot_2 (y, y_, dy, tst, trn);

  xlabel (sprintf('%s',Yname));
  ylabel (sprintf('predicted %s', Yname));
  axis tight
  v = axis();
  axis([v(1:2) 0 v(4)]);
  legend(ht(1), cotyp{i});
endfor

##
# This plot shows the relative weight of each variable in the mean function
# and the relevance in the covariance function.
figure (3); clf;

function do_plot_3 (ell, Xname)
  L   = sum (ell);
  if L > sqrt(eps)
    ell ./= L;
  endif
  [ell orel] = sort (ell, 'descend');
  dim = length (ell);
  bar (1:dim, ell, 'r');
  set (gca, 'xticklabel', Xname(orel))
endfunction

sp = [1,3;2,4];
for i=1:2
  hyp = HYP.(cotyp{i});

  subplot (2, 2, sp(i,1))
  if !isempty (hyp.mean)
    ell = abs (hyp.mean(1:dim));
  else
    ell = zeros(dim,1);
  endif
  do_plot_3 (ell, Xname)
  if i == 1; ylabel ('Relevance [mean]'); endif
  axis tight
  v = axis();
  axis([v(1:2) 0 v(4)]);
  legend(cotyp{i});

  subplot (2, 2, sp(i,2))
  # inverse ARD lengths
  ell2 = exp (-2 * hyp.cov(1:dim));
  do_plot_3 (ell2, Xname)
  if i ==1; ylabel ('Relevance [cov]'); endif
  legend(cotyp{i});
endfor
