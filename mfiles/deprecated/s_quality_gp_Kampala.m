# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

## Gaussian Process regression
# We model the quality outputa using Gaussian Process regression
#
Xname = {'CoVol','CoAge','NUsers','TrVol','SEmptyW','OrCat'};
Yname = 'NH4N';
[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');
Xcat = X(:, isXcat);
ncat = size (Xcat, 2);
assert (ncat, 1);
idx_cat = categorypartition (Xcat);
X = X(:,!isXcat);
Xname = Xname(!isXcat);

## Decorrrelate input variables
# Instead of the raw inputs we use their log PCA as inputs
#
x = log10 (X);
[x xmean xstd] = zscore (x);
[u s v] = svd (x, 1);
M   = s * v.';
Mri = v * diag (1 ./ diag (s)); # right inverse of M
val2data = @(x) (x * M) .* xstd + xmean; # convert pca values to data space
data2val = @(x) (x - xmean)./xstd * Mri; # convert data space to pca values

x = u;
y = log10 (Y);

[N dim] = size (x);
if !exist('trn','var')
  [xtrn xtst trn tst] = splitdata (x, N-20);
  ytrn = y(trn);
  ytst = y(tst);
endif

## GP regressor
#
pkg load gpml
meanf = {@meanSum, {@meanLinear, @meanConst}};
covf = {@covMaternard,1};
likf = @likGauss;

if !exist('hyp','var')
  hyp.lik  = -0.9;
  hyp.mean = 3 * randn (dim+1,1);
  hyp.cov  = randn(dim+1,1);
endif
prior.lik = {{@priorSmoothBox1,-1,3,2}};
args = {{@infPrior,@infLOO,prior}, meanf, covf, likf, xtrn, ytrn};

%hyp = minimize (hyp, @gp, -1e3, args{:});
evalc('hyp = minimize (hyp, @gp, -1e3, args{:})');
nlml = gp (hyp, args{:});
[y_ dy2] = gp(hyp, args{:}, x);
dy = 1.2 * sqrt (dy2);

# Compute actual LOO uncertainty
if !exist('dy_loo','var')
  dy2 = zeros (N,1);
  args_ = args;
  for j=1:N
    [~,~,idxt,idxv] = splitdata ([x y], setdiff(1:N,j));
    args_(5:6)       = {x(idxt,:), y(idxt)};
    [~,dy2(j)]      = gp(hyp, args_{:}, x(idxv,:));
  endfor
  dy_loo = 1.2 * sqrt (dy2);
endif

printf ("Results for %s\n", Yname);
printf ("Negative log marginal likelihood %s: %.3f\n", Yname, nlml);
printf ("RMSE in test set (%d): %.3f\n", length(tst), sqrt(mean((ytst-y_(tst)).^2)));
fflush (stdout);

figure (1)
clf
subplot(2,1,1)
hold on
hv = errorbar (ytrn, y_(trn), dy_loo(trn), '~or');
ht = errorbar (ytrn, y_(trn), dy(trn), '~ob');
set(hv,'markerfacecolor','auto','markersize', 0.3*get(ht,'markersize'))
hv(2) = errorbar (ytst, y_(tst), dy_loo(tst), '~or');
ht(2) = errorbar (ytst, y_(tst), dy(tst), '~og');
set(hv(2),'markerfacecolor','auto','markersize', 0.3*get(ht(2),'markersize'))
hold off
line([min(y) max(y)], [min(y) max(y)],'color','k');
xlabel (sprintf('log10(%s)',Yname));
ylabel (sprintf('predicted log10(%s)', Yname));
axis tight

subplot(2,1,2)
# inverse ARD lengths
ell2 = diag(Mri * diag(exp(-2*hyp.cov(1:dim))) * Mri.');
L    = sum (ell2);
ell2 ./= L;
[ell2 orel] = sort (ell2, 'descend');
bar (1:dim, ell2, 'r');
axis tight
set (gca, 'xticklabel', Xname(orel))
ylabel ('Relative relevance');

figure (2)
clf
ncat   = numel(idx_cat);
nL     = 20;
m      = arrayfun (@(i)median(y(idx_cat{i})), 1:ncat);
[m, o] = sort (m);

symb = 'opv*+^';
col = flipud (viridis (ncat));
hold on
for i=1:ncat
  j = idx_cat{o(i)};
  hd(i) = loglog (10.^y(j), 10.^y_(j), symb(j), 'markerfacecolor', 'auto');
  set(hd(i),'color', col(i,:));
endfor
hold off
ly = [min(10.^y) max(10.^y)];
line(ly, ly,'color','k');
xlabel (sprintf('%s',Yname));
ylabel (sprintf('predicted %s', Yname));
axis tight
Xcat_str.OrCat{1} = 'I-C-I';
Xcat_str.OrCat{2} = 'P.toilet';
Xcat_str.OrCat{3} = 'H+';
Xcat_str.OrCat{5} = 'H';
Xcat_str.OrCat{4} = 'Sc';
Xcat_str.OrCat{6} = 'R/Ho';
lbl = Xcat_str.OrCat(o);
for i=1:numel(lbl)
  lbl{i} = sprintf ("%s (%.1f)", lbl{i}, m(i));
endfor
legend (hd, lbl, 'Location','NorthOutside',...
                            'Orientation', 'Horizontal')
