# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

## Gaussian Process regression
# We model the quality outputa using Gaussian Process regression with the
# Matern covariance function

Xname = {{'CoVol','NUsers','CoAge','OrCat'},{'CoVol','NUsers','CoAge','OrCat'}};
Yname = {'TS','TS'};
City  = {'Kampala', 'Hanoi'};

for c=1:2
  [tmpX tmpY isXcat Xcat_str] = dataset (Xname{c}, Yname{c}, City{c});
  Xcat = tmpX(:, isXcat);
  idx_cat = categorypartition (Xcat);
  # Select only Households
  [tf,i] = ismember ({'Household', 'Multiple Household'}, Xcat_str.OrCat);
  if length(i(tf)) > 1
    idx_household = cat (idx_cat(i(tf)){:});
  else
    idx_household = idx_cat{i(tf)};
  endif
  X{c} = tmpX(idx_household, !isXcat);
  Y{c} = tmpY(idx_household);
endfor
Xname = Xname{1}(!isXcat);
%X{1}(:,1) = X{1}(:,1) *  52.1429; # conver years to weeks

## Plot the two sets
yname = 'TS';
figure (1)
clf
np = 15;
sp = {4:3:3*np, 5:3:3*np, 6:3:3*np};
[tf,k] = ismember ({'CoVol', 'NUsers', 'CoAge'}, Xname);
for i=1:3
  subplot(np,3,sp{i})
  h1 = loglog (X{1}(:,k(i)), Y{1}, 'o','markerfacecolor','auto');
  hold on
  h2 = loglog (X{2}(:,k(i)), Y{2}, 'x');
  axis tight
  grid on
  xlabel (Xname{k(i)})
  if i == 1
    ylabel (yname);
  endif
  hold off
endfor
subplot(np,3,1:3)
title('Households');
axis off
legend([h1,h2], City,'Location','North','Orientation','Horizontal');

% all the regression is performed on nonlinearly transformed variables
x = log10 (cell2mat(X.'));
x = zscore (x);
[N dim] = size (x);
y = (cell2mat (Y.'));
yname = 'TS';

### GP regressor
##
pkg load gpml
meanf = {@meanSum, {@meanLinear, @meanConst}};
covf = {@covMaternard,1};
likf = {@likGauss};

##
# To avoid over-fitting we set a prior on the noise parameter that penalizes
# values that are unrealistically low.
prior.lik = {{@priorSmoothBox1,-3,3,40}};
##
# Prior on the slope of the mean function
#prior.mean = cell(dim+1,1);
#prior.mean{2} = {@priorSmoothBox1,-2,-0.05,40};
args = {{@infPrior,@infExact,prior}, meanf, covf, likf, [], []};

% If the hyper-parameters are not set, set some good initial values
if !exist ('hyp', 'var')
  hyp.lik  = -3; % start with a highly regularized model
  hyp.mean = [-0.2; -0.1; 0.1; 1.27] + 0.5 * randn (dim+1,1);
  hyp.cov  = zeros (dim+1,1);
endif

% Training and test set.
% We select test points evenly spaced in the TS scale
ntest = 20;
[N dim] = size (x);
[yo o]  = sort (y);
idx     = o(round (linspace (2, N-1, ntest)));

[xtst xtrn tst trn] = splitdata (x, idx);
ytrn = y(trn);
ytst = y(tst);

% input arguments for the regression function
args{5} = xtrn; % training inputs
args{6} = ytrn; % training outputs

% If the variable _calc_loo_ is defined then we will manually calculate the
% leave one out prediction error.
if exist('calc_loo', 'var')
  [hyp, dy_loo] = train_gp (x, y, args, hyp);
else
  hyp = train_gp (x, y, args, hyp);
endif

% Print some summary of the results
nlml     = gp (hyp, args{:});
[y_ dy2] = gp (hyp, args{:}, x);
dy       = 1.96 * sqrt (dy2);
rmse_test = sqrt (meansq (ytst - y_(tst)));
printf ("\nResults for %s\n", yname);
printf ("Negative log likelihood %s: %.3f\n", yname, -nlml);
printf ("RMSE in test set (%d): %.3f\n", length(tst), rmse_test);
fflush (stdout);

figure (2); clf
ht = errorbar (y(trn), y_(trn), dy(trn), '~ob');
hold on
ht(2) = errorbar (y(tst), y_(tst), dy(tst), '~og');
hold off
line([min(y) max(y)], [min(y) max(y)],'color','k');
xlabel (sprintf('%s',yname));
ylabel (sprintf('predicted %s', yname));
axis tight

figure(3); clf
function do_plot_3 (ell, Xname)
  L   = sum (ell);
  if L > sqrt(eps)
    ell ./= L;
  endif
  [ell orel] = sort (ell, 'descend');
  dim = length (ell);
  bar (1:dim, ell, 'r');
  set (gca, 'xticklabel', Xname(orel))
endfunction

subplot (2, 1, 1)
if !isempty (hyp.mean)
  ell = abs (hyp.mean(1:dim));
else
  ell = zeros(dim,1);
endif
do_plot_3 (ell, Xname)
ylabel ('Relevance [mean]');
axis tight
v = axis();
axis([v(1:2) 0 v(4)]);

subplot (2, 1, 2)
# inverse ARD lengths
ell2 = exp (-2 * hyp.cov(1:dim));
do_plot_3 (ell2, Xname)
ylabel ('Relevance [cov]');

figure(4); clf;

function ht = do_plot_4 (y, y_, dy, tst)
  [~, o] = sort (y_, 'descend');
  [~, i] = ismember (tst, o);
  ht = plot (y(o), 'ob', i, y_(o)(i), 'og');
  hold on
  hv = errorbar (1:max(o), y_(o), dy(o), '~or');
  hv(2) = errorbar (i, y_(o)(i), dy(o)(i), '~.g');
  set(hv(1), 'markerfacecolor', 'auto', 'markersize', 0.5*get(ht(1), 'markersize'))
  hold off
endfunction

ht = do_plot_4 (y, y_, dy, tst);
axis tight
%v = axis();
%axis([v(1:2) 0 v(4)]);
xlabel(sprintf('%s rank (predicted)',yname))
ylabel(yname)

