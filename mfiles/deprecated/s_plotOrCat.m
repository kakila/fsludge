# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

## Gaussian Process regression
# We model the quality outputa using Gaussian Process regression
#
Xname = {'OrCat'};
Yname = {'NH4N','COD'};
for i=1:numel(Yname)
  [X Y isXcat Xcat_str] = dataset (Xname, Yname{i}, 'Kampala');
  [Y o] = sort (Y,'ascend');
  idx_cat = categorypartition (X(o));

  lbl{1} = 'I-C-I';
  lbl{2} = 'P.toilet';
  lbl{3} = 'H+';
  lbl{5} = 'H';
  lbl{4} = 'Sc';
  lbl{6} = 'R/Ho';
  symb   = 'op^*v+';

  figure(i)
  clf
  ncat = numel (idx_cat);
  %hold on
  for k=1:ncat
    j = idx_cat{k};
    %hd(k) = loglog (j,Y(j), symb(k));
    %set(hd(k), 'markerfacecolor', 'auto','markersize',3);
    Yc{k} = Y(j);
  endfor
  boxplot (Yc,1);
  %hold off
  xlabel ('Origin Category');
  set (gca, 'xtick', 1:ncat, 'xticklabel', lbl, 'yscale', 'log');
  ylabel (Yname{i});
  axis tight
  %legend (hd, lbl, 'Location','NorthOutside',...
  %                            'Orientation', 'Horizontal')
endfor % over Yname
