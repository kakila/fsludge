# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load gpml

## Gaussian Process regression of TS
# We load continuous site variables to build a model for TS
#
Xname = {'CoVol','CoAge','NUsers'};
Yname = 'TS';
[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Hanoi');

% all the regression is performed on nonlinearly transformed variables
x = log (X);
x = zscore (x);
[N dim] = size (x);

y = Y;

## GP regressor
# The regressor uses a linear mean function.
# We use a Matern covariance function with automatic relevance determination.
#
meanf = {@meanSum, {@meanLinear, @meanConst}};
covf = {@covMaternard,1};
likf = {@likGauss};

##
# To avoid over-fitting we set a prior on the noise parameter that penalizes
# values that are unrealistically low.
prior.lik = {{@priorSmoothBox1,2,5,40}};
args = {{@infPrior,@infExact,prior}, meanf, covf, likf, [], []};
## Regress the data
#
y_model = dy_model = idx_tst = idx_trn = struct ();
if !exist('hyp', 'var')
  hyp.lik  = 5; % start with a highly regularized model
  hyp.mean = [-8; 2; -1; 30] + 3 * randn (dim+1,1);
  hyp.cov  = [11; -1; -3; 2] + 1 * randn (dim+1,1);
endif

% Training and test set.
% We select test points evenly spaced in the TS scale
ntest = 20;
[N dim] = size (x);
[yo o]  = sort (y);
idx     = o(round (linspace (2, N-1, ntest)));

[xtst xtrn tst trn] = splitdata (x, idx);
ytrn = y(trn);
ytst = y(tst);

% input arguments for the regression function
args{5} = xtrn; % training inputs
args{6} = ytrn; % training outputs

% If the variable _calc_loo_ is defined then we will manually calculate the
% leave one out prediction error.
if exist('calc_loo', 'var')
  [hyp, dy_loo] = train_gp (x, y, args, hyp);
else
  hyp = train_gp (x, y, args, hyp);
endif

% Print some summary of the results
nlml     = gp (hyp, args{:});
[y_ dy2] = gp (hyp, args{:}, x);
dy       = 1.96 * sqrt (dy2);
rmse_test = sqrt (meansq (ytst - y_(tst)));
printf ("\nResults for %s\n", Yname);
printf ("Negative log likelihood %s: %.3f\n", Yname, -nlml);
printf ("RMSE in test set (%d): %.3f\n", length(tst), rmse_test);
fflush (stdout);

## Plot results
# These plots illustrate the performance of the model
#

figure(1); clf;

function ht = do_plot_1 (y, y_, dy, tst)
  [~, o] = sort (y_, 'descend');
  [~, i] = ismember (tst, o);
  ht = plot (y(o), 'ob', i, y_(o)(i), 'og');
  hold on
  hv = errorbar (1:max(o), y_(o), dy(o), '~or');
  hv(2) = errorbar (i, y_(o)(i), dy(o)(i), '~.g');
  set(hv(1), 'markerfacecolor', 'auto', 'markersize', 0.5*get(ht(1), 'markersize'))
  hold off
endfunction

ht = do_plot_1 (y, y_, dy, tst);
axis tight
v = axis();
axis([v(1:2) 0 v(4)]);
xlabel(sprintf('%s rank (predicted)',Yname))
if i == 1; ylabel(Yname); endif

figure (2); clf;

function ht = do_plot_2 (y, y_, dy, tst, trn)
  ht = errorbar (y(trn), y_(trn), dy(trn), '~ob');
  hold on
  ht(2) = errorbar (y(tst), y_(tst), dy(tst), '~og');
  #if exist('dy_loo', 'var')
  #  hv = errorbar (ytrn, y_(trn), dy_loo(trn), '~or');
  #  hv(2) = errorbar (ytst, y_(tst), dy_loo(tst), '~or');
  #  set(hv(1),'markerfacecolor','auto','markersize', 0.5*get(ht(1),'markersize'))
  #  set(hv(2),'markerfacecolor','auto','markersize', 0.3*get(ht(2),'markersize'))
  #endif
  hold off
  mM = [min(y) max(y)];
  line (mM, mM,'color','k');
endfunction

ht = do_plot_2 (y, y_, dy, tst, trn);

xlabel (sprintf('%s',Yname));
ylabel (sprintf('predicted %s', Yname));
axis tight
v = axis();
axis([v(1:2) 0 v(4)]);

##
# This plot shows the relative weight of each variable in the mean function
# and the relevance in the covariance function.
figure (3); clf;

function do_plot_3 (ell, Xname)
  L   = sum (ell);
  if L > sqrt(eps)
    ell ./= L;
  endif
  [ell orel] = sort (ell, 'descend');
  dim = length (ell);
  bar (1:dim, ell, 'r');
  set (gca, 'xticklabel', Xname(orel))
endfunction

subplot (2, 1, 1)
if !isempty (hyp.mean)
  ell = abs (hyp.mean(1:dim));
else
  ell = zeros(dim,1);
endif
do_plot_3 (ell, Xname)
if i == 1; ylabel ('Relevance [mean]'); endif
axis tight
v = axis();
axis([v(1:2) 0 v(4)]);

subplot (2, 1, 2)
# inverse ARD lengths
ell2 = exp (-2 * hyp.cov(1:dim));
do_plot_3 (ell2, Xname)
if i ==1; ylabel ('Relevance [cov]'); endif
