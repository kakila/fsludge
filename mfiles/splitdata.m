## Copyright (C) 2018 Juan Pablo Carbajal
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.

## Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
## Created: 2018-01-16

## -*- texinfo -*-
## @defun {[@var{v1} @var{v2} @var{idx1} @var{idx2}]} =} splitdata (@var{v}, @var{n} = 0.8)
## @defunx {[@dots{}]} =} splitdata (@dots{}, @var{dim} = 1)
## Splits data into two complementary proper subsets.
##
## @var{n} if scalar < 1 then is fraction of total points in @var{v1}
##
## @var{n} if scalar >= 1 then is number of elements in @var{v1}
##
## @var{n} if array with length > 1 then it is the indexes of elements in @var{v1}
##
## @var{n} if logical array with length == length(x) then is selection mask of
## elements in @var{v1}
##
## Optional argument @var{dim} indicates which dimension of @var{v} to split.
##
## @seealso{randperm}
## @end defun

function [vt vs idx idxs] = splitdata (v, idx = 0.8, dim = 1)

  do_perm = false;
  if dim ~= 1
    perm_vec = [dim setdiff(1:ndims(v), dim)];
    v = permute (v, perm_vec);

    sz = size (v);
    n  = sz(1);
    md = prod (sz(2:end));
    v  = reshape (v, n, md);

    do_perm = true;
  endif

  n = size (v,1);

  if isscalar (idx) && !islogical (idx)
    if idx < 1
      idx = randperm (n, max (round (idx * n), 1));
    else
      idx = randperm (n, min (idx, n-1));
    endif
  endif

  vt = v(idx,:);
  if islogical (idx)
    idxs = !idx;
  else
    idxs = setdiff (1:n, idx);
  endif
  vs = v(idxs,:);

  if do_perm
    # put the vectors bck to the orignal shape
    nt = numel (idx);
    vt = reshape(vt, nt, sz(2:end))
    vt = permute (vt, perm_vec);

    ns = numel (idxs);
    vs = reshape(vs, ns, sz(2:end));
    vs = permute (vs, perm_vec);
  endif

endfunction

%!shared v
%! x = [1 2 3].';
%! y = [1 0 -1].';
%! v= [x y];

%!test splitdata (v, 1);
%!test splitdata (v, 0.1);
%!assert (splitdata(v, [false true false]), v(2,:))
%!assert (splitdata(v, [1 3]), v([1 3],:))

%!demo
%! x = (0:10).';
%! y = rand (size (x));
%! v = [x y];
%! [v1 v2 idx idx2] = splitdata (v,4);
%!
%! n1 = length (v1); n2 = length (v2);
%! [N k] = max([n1,n2]); n = min(n1,n2);
%! v12 = {v1(:,1), v2(:,1)}; v12=v12([k 3-k]);
%! idx = {idx,idx2}; idx = idx([k 3-k]);
%! printf ('  i  x1 j  x2\n')
%! for i=1:N
%!  if i <= n
%!    printf ('%3d%3d%3d%3d\n', idx{2}(i), v12{2}(i), idx{1}(i), v12{1}(i));
%!  else
%!    printf ('  ·  ·%3d%3d\n', idx{1}(i), v12{1}(i));
%!  endif
%! endfor
