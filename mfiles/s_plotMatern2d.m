## Plot of Matérn covariance function in 2D
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load gpml

x = sort([0; (logspace(-3, 0, 100).'.*[-1 1])(:)]);
[X Y] = meshgrid (x);

ell = [3; 0.25]; # charactersitic lengths
hyp = [log(ell); 0];

xy = [X(:) Y(:)];
z  = covMaternard (1, hyp, xy, [0 0]);
Z  = reshape (z, size(X));

figure (1), clf
  surf (X, Y, Z, 'edgecolor', 'none')
#  hold on
#  [~,h] = contour3 (X,Y,Z, linspace(0,1,50).');
#  set (get(h,'children'), 'edgecolor', 'k')
  axis tight
  zlim([-0.01 zlim()(2)])
  axis equal
#  shading interp
#  light
  xlabel ('X')
  ylabel ('Y')
  zlabel ('cov')
  view(-44, 20);

figure (2), clf
  #waterfall (X, Y, Z)
  contour3 (X,Y,Z, linspace(0,1,50).');
  axis tight
  axis equal
  xlabel ('X')
  ylabel ('Y')
  zlabel ('cov')

