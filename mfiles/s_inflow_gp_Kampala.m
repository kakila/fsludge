## Gaussian process regression inflow from Kampala separated by origin category
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

pkg load gpml

## Load data
# We load continuous site variables to build a model for the inverse of the
# emptying period |SEmptyW|.
# We also load the categorical variable container type to segment the dataset.
# The latter is then removed from the input variables |Xname|.
# |Yname| contains the output variable to be predicted.
#
# Since the origin category |OrCat| can only be a cause of the observed variables
# we believe that we do not introduce bias in this way, in any case we are
# avoiding confounding.
#
Xname = {'NUsers','CoVol', 'CoAge', 'TrVol', 'IC', 'CoTyp'};
Yname = 'SEmptyW';

[X Y isXcat Xcat_str] = dataset (Xname, Yname, 'Kampala');

X         = X(:,!isXcat);        # select non-categorical variables
Xname     = Xname(!isXcat);      # get their names
Xcat      = X(:, isXcat);        # select categorical variables
Xname_cat = Xname(isXcat);       # get their names

% Indexes to partition output using the categorical variable
idx_cat = categorypartition (Xcat);
cotyp   = Xcat_str.(Xname_cat{1}); %{'Pit latrine', 'Septic tank'};

% Indexes of variables used for mean function
[~, imean] = ismember ({'NUsers', 'CoVol'}, Xname);

## GP regressor
# All the regression is performed on logarithmic transformed variables.
# We take the negative logarithm of |SEmptyW| to get the frequency.
# After we are in the space where the regression will take place we normalize
# the input variables to put them all in similar scales:
#
# $$ y = -\log(Y) $$
# $$ x_i = log (X_i) $$
# $$ x_i = \frac{x_i - \tilde{x}_i}{\alpha_{x_i}} $$
#
# The GP structure is defined in the function
# <https://bitbucket.org/KaKiLa/fsludge_pub/src/tip/mfiles/inflowgp.m |inflowgp.m|>,
# refer to it to know more details.
#
if !exist('HYP', 'var')
  HYP = ARG = struct();
endif

% Verbosity is true, define the variable verbose in the command line to override
% Make sure verbose is false when generating a html report with publish
if ~exist ('verbose', 'var')
  verbose = false;
endif

% Loop over origin categories and build a model for each
for icat = 1:2
  cat_name = cotyp{icat};

  x = log10 (X(idx_cat{icat},:));
  x = x - median (x);
  x = x ./ mean (abs (x));
  y = -log10 (Y(idx_cat{icat})); % -log Period = log Freq

  if !isfield (HYP, cat_name)
    hyp = [];
  else
    hyp = HYP.(cat_name);
  endif

  % Choose hyper-parameter constraints for each category
  % log of the error bounds: 1/7-100 week
  Ferror = sort (log (abs ([-log10(1/7) -log10(100)])));
  switch cat_name
    case 'Pit latrine'
      maxcov = log (0.06); % Max correction should be about 10% of mean
    case 'Septic tank'
      maxcov = log (0.122); % Max correction should be about 10% of mean
  endswitch
  [hyp args] = inflowgp (x, y, imean, hyp, Ferror, maxcov, verbose);

  % Store results for further plotting
  y_data.(cat_name) = y;
  HYP.(cat_name)    = hyp;
  ARG.(cat_name)    = args;
  XX.(cat_name)     = X(idx_cat{icat},:);
  YY.(cat_name)     = 1./Y(idx_cat{icat},:);
endfor % over categories

## Summary of results
# The coefficient of variation is computed as the ratio between the predictive
# standard deviation and the predictive mean.
#
# $$ c(\vec{x}) = \frac{\sigma_y(\vec{x})}{\bar{y}(\vec{x})} $$
#
# It is used to quantify the amount of correction.
#
# Since for emptying frequency we have a prior model, the correction was
# constrained to produce a maximum coefficient of variation of about 10%.
#
yname = 'Frequency [1/week]';
xname = {'# users', 'Containment V.', 'Containment age', ...
  'Truck V.', 'Income level'};
for icat = 1:2
  cat_name = cotyp{icat};
  printf ('\n-- %s --\n', cat_name);
  printf ('Mean func vars:')
  printf (' %s,', xname{imean}); printf('\n');
  printf ('Cov func vars:')
  printf (' %s,', xname{:}); printf('\n');
  report_gp (HYP.(cat_name), ARG.(cat_name), @(x)10.^(x));
  % For tables
  printf('%s\n', cat_name)
  printf(' & %.4e', HYP.(cat_name).mean.'); printf('\n');       % mean
  printf(' & %.4e', exp(HYP.(cat_name).cov.')); printf('\n');   % cov
  variance = exp(HYP.(cat_name).lik.'); variance = variance(1) * variance(2)^2 / (variance(1)-2);
  printf(' & %.4e', exp(HYP.(cat_name).lik.'), sqrt(variance)); printf('\n');   % lik
endfor

## Plot results
# These plots illustrate the performance of the model
#
plotresults_gp (1, HYP, ARG, XX, YY, xname, {'log10', yname}, @(x)10.^(x));
for fig =3:4
  h = get(figure(fig), 'children');
  for i=1:length(h)
    axes(h(i));
    set (h(i), 'yscale', 'log', 'ygrid', 'on');
    set (h(i), 'xscale', 'log', 'xgrid', 'on');
    axis tight
    xticks ([]); xticks ("auto"); % force recalculation of ticks
    yticks ([]); yticks ("auto"); % force recalculation of ticks
    drawnow
  endfor
endfor
print (2, 'Fig8_Kampala_EFreq_output.png');

##
# This plot shows the relative weight of each variable in the mean function
# and the relevance in the covariance function.
plothypARD (7, HYP.(cotyp{1}), xname, imean);
subplot (2,1,1); title (cotyp{1})
print (7, 'Fig7a_Kampala_EFreq_covcoeffs.png');
plothypARD (8, HYP.(cotyp{2}), xname, imean);
subplot (2,1,1); title (cotyp{2})
print (8, 'Fig7b_Kampala_EFreq_covcoeffs.png');
