## Gaussian process regression TS from Hanoi households
#
##

# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Created: 2018-01-09

## Dependencies
#
pkg load gpml

## Load data
# |Xname| contains the variables used as predictor
# |Yname| contains the output variable
Xname = {'NUsers','CoVol', 'SludgeAge', 'Vpumped'};
Yname = 'TS';
[X Y] = dataset (Xname, Yname, 'Hanoi');

## GP regressor
# We use the data directly, without non-linear mappings.
# We only center to the median and rescale with the mean absolute deviation from
# the median
#
# $$ x_i = \frac{X_i - \tilde{X}_i}{\alpha_{X_i}} $$
#
y           = Y;
x           = X - median (X);
x           = x ./ mean ( abs (x) );
assert (all (isfinite (x)))

##
# Variables that are particularly not useful for prediction are removed from the
# mean and covariance functions, respectively.
# |imean| and |icov| are used as a selection mask. The indices inside these variables
# refer to positions in |Xname|
imean       = setdiff (1:length(Xname), 4); % remove Vpumped from mean
icov        = setdiff (1:length(Xname), 2); % remove CoVol from cov
N           = length (y);

##
# Since we do not have a prior model structure hyper-parameters are difficult to
# constrain.
# For the exploration of the hyper-parameters we split the data in training and
# test sets.
# After good parameters are found, we use the whole dataset for training.
# This hopefully increases the significance of the regressed parameters.
#

%comment and clear _idx_ to have a test set.
idx = struct ('trn', 1:N, 'tst', []);
if !exist ('idx', 'var')
  N   = length (x);
  idx = struct ('trn', [], 'tst', []);
  [~,~,idx.tst,idx.trn] = splitdata ([x y], 0.2);
  % add min and max values
  [~,imin] = min (y);
  [~,imax] = max (y);
  idx.tst = sort (setdiff (idx.tst, [imin imax]));
  idx.trn = sort ([idx.trn imin imax]);
endif

% Verbosity is true, define the variable verbose in the command line to override
% Make sure verbose is false when generating a html report with publish
if !exist ('verbose', 'var')
  verbose = false;
endif
if !exist('hyp', 'var')
  hyp = [];
endif

##
# The GP structure is defined in the function
# <https://bitbucket.org/KaKiLa/fsludge_pub/src/tip/mfiles/TSgp.m _TSgp.m_>, refer to
# it to know more details.
#

% Train
[hyp args] = TSgp (x(idx.trn,:), y(idx.trn), {imean, icov}, hyp, ...
  [log(1) log(10)], log(1e1), verbose);
% Test
[y_, dy2_, f_, df2_, lp, post] = gp (hyp, args{:}, x, y);
dy_ = 1.96 * sqrt (dy2_);

## Summary of the results
# The coefficient of variation is computed as the ratio between the predictive
# standard deviation and the predictive mean.
#
# $$ c(\vec{x}) = \frac{\sigma_y(\vec{x})}{\bar{y}(\vec{x})} $$
#
# It is used to quantify the amount of correction.
# Since we do not have a prior model for |TS|, this correction indicates
# nonlinear terms that could be used to improve prediction.
#
yname = 'TS [g/L]';
xname = {'# users','Containment V', 'Sludge age', 'Pumped V.'};

printf ('Mean func vars:')
printf (' %s,', xname{imean}); printf('\n');
printf ('Cov func vars:')
printf (' %s,', xname{icov}); printf('\n');
report_gp (hyp, args);
% For tables
printf('Septic tank\n')
printf(' & %.4e', hyp.mean.'); printf('\n');       % mean
printf(' & %.4e', exp(hyp.cov.')); printf('\n');   % cov
printf(' & %.4e', exp(hyp.lik.')); printf('\n');   % lik

## Plot results
# These plots illustrate the performance of the model
#
plotresults_gp (1, hyp, args, X(idx.trn,:), Y(idx.trn,:), xname, {'',yname});
figure(2)
  ylim ([0 ylim()(2)]);
  title ('Septic tank')
if !isempty (idx.tst)
  hold on
  errorbar (y(idx.tst), y_(idx.tst), dy_(idx.tst), '~or')
  hold off
  axis tight
endif
print (2, 'Fig2_Hanoi_TS_output.png');

##
# This plot shows the relative weight of each variable in the mean function
# and the relevance in the covariance function.
plothypARD (4, hyp, xname,{imean, icov});
figure(4)
  subplot (2,1,1); title ('Septic tank')
print (4, 'Fig1_Hanoi_TS_covcoeffs.png');


