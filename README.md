# Modelling quantities and qualities of faecal sludge for improved management solutions

This is the source code repository for the analyses reported in the article
[Modelling quantities and qualities (Q&Q) of faecal sludge in Hanoi and Kampala for improved management solutions]()

The repository is maintained by Juan Pablo Carbajal <juanpablo.carbajal@eawag.ch>.
Please do not hesitate to contact us with bug reports (you can also use the [issue tracker](https://gitlab.com/kakila/fsludge/issues)),
questions, and comments.

## Folder structure
The repository has the following structure:

    .
    ├── data       # Data files
    ├── doc        # Some documents used during the development of the project
    ├── mfiles     # Gnu Octave functions and script to reproduce all results
    └── README.md  # This file

The `data` folder includes a filtered version of the whole raw data that can be
obtained from [the data archive](https://doi.org/10.25678/0000TT).

The `doc` folder has working documents that we used during the development of
the project, as well as the Latex sources of the supplementary information
published along with the article.

The `mfiles` folder contains all the scripts needed to reproduce the results
reported in the article.
In this folder, files with prefix `s_` are scripts that will generate reports
and figures of the results.
All other files are functions used to make the scripts simpler and easier to read.

## Dependencies

For the scripts to work you need [GNU Octave 4.4](https://www.gnu.org/software/octave/)
or later. You will also need to install the packages:

* [io](https://octave.sourceforge.io/io/index.html) 4.0.0 or later

* [statistics](https://octave.sourceforge.io/statistics/index.html) 1.4.0 or later

* [gpml](http://www.gaussianprocess.org/gpml/code/matlab/doc/) 4.2.0 or later

### Windows
In Windows, the `io` and `statistics` packages should be already included in your GNU Octave distribution.
If your GNU Octave does include these packages, they should be listed in the output of the following command (run it in the Octave's Command Window):

```octave
pkg list
```

If you do not see `io` or `statistics` in that list it is probable that your installation is incomplete (for example, because you do not have administrator permissions).

#### Install Octave without administrator permissions

To install octave without administrator permissions you can follow these steps:

1. Download the `.zip` or `.7z` from the official [download site](www.octave.org/download) (that is, do not download the installer).
2. Extract the file in the folder of your choice
3. Go to the extracted folder and execute the `post-install.bat` script
4. Start GNU Octave using `octave-firsttime.vbs`

After these steps you should start GNU Octave using `octave.vbs` (e.g. create a link to this file in your desktop).

### Linux
In Linux you can install the `statistics` package running the following command
in the octave prompt (the `io` package is a dependency)

```octave
pkg install -forge io statistics
```

### GPML

The `gpml` package can be installed in linux and windows with the following command

```octave
pkg install https://gitlab.com/hnickisch/gpml-matlab/uploads/e8bafa6308e81a1e61b8da51fef1a95a/gpml-4.2.0.tar.gz
```

which downloads a development release of the official [gpml package](https://gitlab.com/hnickisch/gpml-matlab/tree/octave) for GNU Octave.

## Reproduce results

Figures are generate by scripts (m-files with prefix `s_`). Here is the list:

* Figure 1 and 2: `s_TS_gp_Hanoi.m`
* Figure 3 and 4: `s_TS_gp_Kampala.m`
* Figure 5 and 6: `s_inflow_gp_Hanoi.m`
* Figure 7 and 8: `s_inflow_gp_Kampala.m`
* Figure 9 and 10: `s_inflow_gp_Household.m`

The script `s_inflow_constrained_correction.m` reports results on the effect of
the constrained amplitude of the covariance function.
The covariance function is used as a correction term, hence its amplitude is
constrained to be smaller then the mean function for each input.
You can generate the report with more details running

```octave
publish s_inflow_constrained_correction.m
```

This will generate an `html` folder with the report in html format, which you
can read in your web browser.
You can do the same for all scripts mentioned here.

Plots in the supplementary material can be reproduced with the scripts:

* Exploratory and independence analysis: `s_TS_exploratory_Kampala.m`
* Containment volume per user: `s_volpercapita_Kampala.m`
